-- LUALOCALS < ---------------------------------------------------------
local minetest, sz_pos, sz_table
    = minetest, sz_pos, sz_table
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- FLAME REGISTRATION HELPERS

-- Helper to do basic flame validity check.  An artificial flame must
-- be standing atop a node that provides the correct flame source type.
-- If not, it's extinguished.

local function flame_check(pos)
	pos = sz_pos:new(pos)
	local def = pos:nodedef()
	if not def or not def.name or not def.groups[modname .. "_flame"] then
		return
	end
	if not pos:fire_allowed() then
		pos:node_set()
		return
	end
	local below = pos:add(sz_pos.dirs.d)
	if not below:groups()[modname .. "_flame_source_" ..
	def.name:sub(modname:len() + 2)] then
		pos:node_set()
		return
	end
end

-- Helper to register the actual flame type itself.

local function regflame(name, def)
	minetest.register_node(name, sz_table.mergedeep(def, {
				description = "Fire",
				drawtype = "plantlike",
				light_source = 14,
				groups = {
					[modname .. "_flame"] = 1,
					flame = 1,
					igniter = 2
				},
				drop = "",
				walkable = false,
				pointable = false,
				buildable_to = true,
				damage_per_second = 4,
				on_construct = function(pos)
					flame_check(pos)
				end,
				after_destruct = function(pos)
					local below = sz_pos:new(pos):add(sz_pos.dirs.d)
					below:node_signal(
						modname .. "_extinguished", below:node_get())
				end,
				[modname .. "_flame_check"] = flame_check
			}))
end

-- Periodically check all our mod flames and make sure their conditions
-- for existence are still met.

minetest.register_abm({
		nodenames = { "group:" .. modname .. "_flame" },
		interval = 1,
		chance = 2,
		action = function(pos)
			return sz_pos:new(pos):node_signal(modname .. "_flame_check")
		end
	})

------------------------------------------------------------------------
-- FLAME NODES

-- A taller super-hot flame, maintained by a venturi burner.

local function hotflame_check(pos)
	flame_check(pos)
	local above = sz_pos:new(pos):add(sz_pos.dirs.u)
	if not above:groups()[modname .. "_flame"]
	and above:fire_allowed() then
		above:node_set({ name = modname .. ":hot_flame_2" })
	end
end
regflame(modname .. ":hot_flame_1", {
		tiles = { modname .. "_flame_1.png" },
		groups = {
			igniter = 9,
			[modname .. "_flame_source_hot_flame_2"] = 1
		},
		damage_per_second = 12,
		after_destruct = function(pos)
			local above = sz_pos:new(pos):add(sz_pos.dirs.u)
			above:node_signal(modname .. "_flame_check", above:node_get())
			local below = sz_pos:new(pos):add(sz_pos.dirs.d)
			below:node_signal(modname .. "_extinguished", below:node_get())
		end,
		on_construct = hotflame_check,
		[modname .. "_flame_check"] = hotflame_check
	})
regflame(modname .. ":hot_flame_2", {
		groups = { igniter = 4 },
		tiles = { modname .. "_flame_2.png" }
	})
