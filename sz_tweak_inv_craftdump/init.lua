-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, sz_pos
    = ipairs, minetest, sz_pos
-- LUALOCALS > ---------------------------------------------------------

local function craftdump(player, pos)
	local inv = player:get_inventory()
	for i = 1, inv:get_size("craft") do
		local stack = inv:get_stack("craft", i)
		stack = inv:add_item("main", stack)
		if stack:get_count() > 0 then
			pos:item_eject(stack)
		end
		inv:set_stack("craft", i, "")
	end
end

local oldpos = {}

local function checkpos(player)
	local name = player:get_player_name()
	local pos = sz_pos:new(player:getpos())
	local op = oldpos[name]
	if op and pos:eq(op) then return end
	oldpos[name] = pos
	craftdump(player, pos)
end

minetest.register_globalstep(function()
		for i, player in ipairs(minetest.get_connected_players()) do
			checkpos(player)
		end
	end)
