-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, next, sz_pos, sz_stuffpile, sz_table, tonumber,
      tostring
    = ipairs, minetest, next, sz_pos, sz_stuffpile, sz_table, tonumber,
      tostring
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

sz_stuffpile.nodename = modname .. ":pile"
local settledist = tonumber(minetest.setting_get(modname .. "_settle_distance")) or 5

------------------------------------------------------------------------
-- LOCAL UTILITY FUNCTIONS

-- Search recursively around a starting point, up to a certain distance away, for
-- spaces not filled by solid (walkable) nodes.  If the starting point is inside
-- a solid node, search for the first non-solid, then continue the search from there.
local search_dirs = {
	sz_pos.dirs.n,
	sz_pos.dirs.s,
	sz_pos.dirs.e,
	sz_pos.dirs.w,
	sz_pos.dirs.d,
}
local function space_search(pos, func)
	pos = sz_pos:new(pos)
	local solid = (pos:nodedef() or { }).walkable
	local q = { pos }
	local seen = { }
	for d = 0, settledist do
		local next = sz_table:new()
		local done = false
		for i, p in ipairs(q) do
			local def = p:nodedef()
			local walk = def and def.walkable
			if def and not walk then
				done = func(p, def)
			end
			if not walk or solid then
				if done then break end
				for i, v in ipairs(search_dirs) do
					local np = p:add(v)
					local nk = tostring(np)
					if not seen[nk] then
						seen[nk] = true
						next:insert(np)
					end
				end
			end
			solid = solid and walk
		end
		if done then break end
		q = next:shuffle()
		if #q < 1 then break end
	end
end

------------------------------------------------------------------------
-- ITEM PILE-OF-STUFF SETTLING

-- Overridable function to create a node to contain the items to settle.
function sz_stuffpile.settle_at(pos, items)
	pos:node_set({ name = sz_stuffpile.nodename, param2 = 1 })
	local inv = pos:meta():get_inventory()
	local final
	for i, v in ipairs(items) do
		final = inv:add_item("main", v)
	end
	return final
end

-- Helper wrapper around sz_stuffpile.settle_at to settle an item at a
-- location, which may have a node already there, which may drop its
-- own items into the pile to be merged.
local function merge(pos, item, def)
	local merged = sz_table:new()

	-- Get the list of drops the existing node will leave
	-- when it's broken to replace it with a stuff pile,
	-- and add it to the list of stuff to settle.
	local drops = minetest.get_node_drops(def.name)
	if drops then
		for i, v in ipairs(drops) do
			if v ~= "" then merged:insert(v) end
		end
	end

	-- Add the new item last, so its leftovers are
	-- what are returned.
	merged:insert(item)

	return sz_stuffpile.settle_at(pos, merged)
end

-- Find the best place to "settle" an item into nodespace.  
function sz_stuffpile.settle_search(pos, item, owner)
	local fallback
	space_search(pos, function(p, def)
			if def.name == sz_stuffpile.nodename then
				-- If we find an existing pile of stuff during our search, try to
				-- add items to it, and keep searching only if we have leftovers.
				item = p:meta():get_inventory():add_item("main", item)
				if item:is_empty() then return true end
			else
				local b = p:add(sz_pos.dirs.d)
				local bd = b:nodedef()
				if not bd or bd.walkable and bd.liquidtype == "none" then
					-- If the node we find is supported by another solid node, such that
					-- stuff could settle on it, settle the item here immediately if it's
					-- a free air space, and only replace the existing buildable node if
					-- no alternative is found.
					if def.name == "air" then
						item = merge(p, item, def)
						if item:is_empty() then return item end
					elseif def.buildable_to and not fallback then
						fallback = function()
							item = merge(p, item, def)
						end
					end
				elseif not fallback then
					-- If the node in question is free-floating, i.e. not supported below,
					-- then if we don't find a better place, fall back on dropping our items
					-- over this space and seeing where they land in the next iteration.
					fallback = function()
						p:item_eject(item)
						item:clear()
					end
				end
			end
		end)

	-- If we failed to use up all of the items, but we have a fallback place to put them,
	-- then try to put them there.
	if fallback and not item:is_empty() then fallback() end

	-- Return any leftover items.
	return item
end
