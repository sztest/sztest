-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_pos, sz_table, sz_util
    = minetest, pairs, sz_pos, sz_table, sz_util
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- FLAMMABLE GAS FLOWING FLUID NODES

-- Flammable gas is completely invisible, odorless and tasteless (duh).
-- I wanted it to have an "air-like" feel, but you can still "swim" in
-- it and I can't seem to disable that, since it's technically a liquid
-- according to the game engine.

local srcname = modname .. ":gas_source"
local flowname = modname .. ":gas_flowing"

local function gasreg(name, def)
	minetest.register_node(name, sz_table.mergedeep(def, {
		description = "Flammable Gas",
		drawtype = "airlike",
		groups = {
			flammable = 1,
			very_flammable = 1,
			[modname] = 1,
			not_in_creative_inventory = 1
		},
		drop = "",
		walkable = false,
		pointable = false,
		diggable = false,
		buildable_to = true,
		drowning = 1,
		paramtype = "light",
		sunlight_propagates = true,
		liquid_alternative_flowing = flowname,
		liquid_alternative_source = srcname,
		liquid_viscosity = 0
	}))
end
gasreg(srcname, {
	liquidtype = "source",
})
gasreg(flowname, {
	liquidtype = "flowing",
	paramtype2 = "flowingliquid"
})

-- Flammable gas must come from a nearby gas-producing node.  If an
-- adjacent gas-producing node is removed then the gas will be removed
-- as well.

minetest.register_abm({
	nodenames = { srcname },
	interval = 1,
	chance = 1,
	action = function(pos)
		pos = sz_pos:new(pos)
		local found = false
		for k, v in pairs(sz_pos.dirs) do
			local p = pos:add(v)
			if p:groups()[modname .. "_source"] then
				found = true
				break
			end
		end
		if not found then pos:node_set() end
	end
})

-- Make flammable gas a little extra volatile; flowing nodes near an
-- ignition source can "deflagrate" and spew flames in all directions,
-- effectively making the flame denser and travel faster.  This makes
-- caving more hazardous (it's hard to outrun the flamefront).

minetest.register_abm({
	nodenames = { flowname },
	neighbors = { "group:igniter" },
	interval = 1,
	chance = 10,
	action = function(pos)
		pos = sz_pos:new(pos)
		if not pos:fire_allowed() then return end
		pos:scan_flood(2, function(p)
			local n = p:node_get().name
			if n == "air" or n == flowname then
				p:node_set({ name = "fire:basic_flame" })
			else
				return false
			end
		end)
	end
})

------------------------------------------------------------------------
-- WEAK IGNITERS AND "VERY FLAMMABLE"

-- Some nodes like the flammable gas are "very flammable" which means
-- the can be set on fire by "weak igniters" that produce only a little
-- heat, like torches.

minetest.register_abm({
	nodenames = { "group:very_flammable" },
	neighbors = { "group:weak_igniter", "group:igniter" },
	interval = 1,
	chance = 2,
	action = function(pos)
		pos = sz_pos:new(pos)
		if not pos:fire_allowed() then return end
		local def = pos:nodedef()
		if def and def.on_burn then
			def.on_burn(pos)
		else
			minetest.set_node(pos, { name = "fire:basic_flame" })
		end
	end
})

sz_util.modify_node("default:torch", {
	groups = { weak_igniter = 1 }
})
