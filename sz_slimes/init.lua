-- LUALOCALS < ---------------------------------------------------------
local bucket, ipairs, math, minetest, pairs, print, sz_pos, sz_table,
      sz_util, tostring
    = bucket, ipairs, math, minetest, pairs, print, sz_pos, sz_table,
      sz_util, tostring
local math_floor, math_random
    = math.floor, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- REGISTER SLIME NODES

-- Table of slimes to spawn based on nearby block type influence.
local slimespawn = { }

-- Function to register a new slime.
local function makeslime(name, protoname, desc, def, bucketimg)
	-- Lookup the prototype node for the slime, and calculate some
	-- other attribute defaults from it.
	local proto = minetest.registered_nodes[protoname]
	if not proto then return end
	desc = desc or proto.description

	-- Calculate node names.
	local flowname = modname .. ":slime_" .. name .. "_flowing"
	local srcname = modname .. ":slime_" .. name .. "_source"

	-- Register slimes to spawn on their prototype node.
	slimespawn[protoname] = srcname
	if proto.liquid_alternative_flowing then
		slimespawn[proto.liquid_alternative_flowing] = srcname
	end
	if proto.liquid_alternative_source then
		slimespawn[proto.liquid_alternative_source] = srcname
	end

	-- Make sure there are enough special_tiles.
	local spt = sz_table.mergedeep({ }, proto.special_tiles or proto.tiles)
	if #spt < 2 then spt[2] = spt[1] end
	
	-- Fill in the node definition, allowing for overrides.
	def = sz_table.mergedeep(def or { }, {
			tiles = proto.tiles,
			special_tiles = spt,
			paramtype = "light",
			walkable = false,
			diggable = false,
			drop = "",
			drowning = 1,
			damage_per_second = proto.damage_per_second and
			proto.damage_per_second > 2 and
			proto.damage_per_second or 2,
			light_source = proto.light_source,
			liquid_alternative_flowing = flowname,
			liquid_alternative_source = srcname,
			liquid_renewable = false,
			liquid_viscosity = 5,
			liquid_range = 4,
			sz_slime_name = name,
			groups = {
				[name] = 1,
				sz_slime = 1,
				liquid = 1,
				caustic = 1,
				not_in_creative_inventory = 1,
				sz_bucket_softcore = 1
			},
		})

	-- Register the flowing liquid node.
	minetest.register_node(flowname, sz_table.merge({
				description = desc .. " Slime",
				drawtype = "flowingliquid",
				paramtype2 = "flowingliquid",
				liquidtype = "flowing",
				}, def))

	-- Register the source liquid node.  Copy the def.groups table
	-- because it would be aliased with the groups used in the
	-- flowing node (sz_table.merge doesn't deep-copy).
	def.groups = sz_table:new(def.groups):copy() 
	def.groups.sz_slime_nucleus = 1
	minetest.register_node(srcname, sz_table.merge({
				description = desc .. " Slime Nucleus",
				drawtype = "liquid",
				liquidtype = "source",
				}, def))

	-- Slimes can be picked up in buckets.
	bucket.register_liquid(
		srcname,
		flowname,
		modname .. ":bucket_slime_" .. name,
		bucketimg or ("bucket.png^(" .. proto.tiles[1]
			.. "^sz_slimes_mask.png^[makealpha:255,3,254)"),
		desc .. " Slime Bucket"
	)
end
makeslime("grass", "default:dirt_with_grass", "Green")
makeslime("drygrass", "default:dirt_with_dry_grass", "Yellow")
makeslime("desert", "default:desert_sand", "Tan")
makeslime("desertstone", "default:desert_stone", "Maroon")
makeslime("dirt", "default:dirt", "Brown")
makeslime("gravel", "default:gravel", "Speckled")
makeslime("stone", "default:stone", "Grey")
makeslime("mossy", "default:mossycobble", "Mossy")
makeslime("snow", "default:snow", "White")
makeslime("sand", "default:sand", "Beige")
makeslime("sandstone", "default:sandstone", "Matte")
makeslime("obsidian", "default:obsidian", "Black")
makeslime("lava", "default:lava_source", "Red", nil, "bucket_lava.png")

------------------------------------------------------------------------
-- SLIME NODE INTERACTION

-- Register groups for slime interaction with other nodes.  We use some
-- heuristics to guess what kinds of interactions should apply, in
-- some cases.

local permeable = 0
local soluble = 0
sz_util.modify_node("*", function(def, name)
		if name:endswith(":air") or name:endswith(":ignore") then return def end
		local gr = def.groups or {}

		-- Slimes are caustic, and this especially manifests in them
		-- "eating" certain other nodes.
		if def.liquidtype == "none" and (gr.flammable
			or def.buildable_to) and not gr.tree then
			gr.sz_slime_soluble = 1
			soluble = soluble + 1
			return def
		end

		-- Slimes can "engulf" a node, if it's "permeable",
		-- allowing slimes to pass through certain cheaper
		-- building materials.
		if gr.snappy or gr.crumbly or name:contains("cobble") then
			gr.sz_slime_permeable = gr.snappy and 1
			or (5 - (gr.crumbly or 0))
			permeable = permeable + 1
		end

		return def
	end)
print(modname .. ": " .. permeable .. " node(s) will be permeable by slime, "
	.. soluble .. " will be soluble")

-- ABM for slimes to eat "slime-soluble" nodes.
minetest.register_abm({
		nodenames = { "group:sz_slime_soluble" },
		neighbors = { "group:sz_slime" },
		interval = 2,
		chance = 5,
		action = function(pos, node)
			-- The node must actually have slime material flowing
			-- "against" it, not merely e.g. passing underneath it
			-- or stopping next to it.
			pos = sz_pos:new(pos)
			local wash, src, srcdef = pos:fluid_washout()
			if wash and srcdef and srcdef.groups
			and srcdef.groups.sz_slime then
				print(modname .. ": " .. srcdef.name .. " dissolved "
					.. pos:node_get().name .. " at " .. tostring(pos))
				pos:node_set()
				pos:smoke()
			end
		end
	})

------------------------------------------------------------------------
-- SLIME AI / MOVEMENT

-- For some nodes that are "permeable" by a slime, we allow it to take
-- over that space, but the original node will be replaced if it moves
-- away (or is killed).  For that, we use a special "sentinel" entity
-- that we place to watch that spot, and replace the original node data
-- if the slime moves.
local entname = modname .. ":slime_node_sentinel"
minetest.register_entity(entname, {
		initial_properties = {
			hp_max = 1,
			physical = false,
			collide_with_objects = false,
			collisionbox = { 0, 0, 0, 0, 0, 0 },
			is_visible = false,
		},
		get_staticdata = function(self)
			self.data = self.data or {}
			return minetest.serialize(self.data)
		end,
		on_activate = function(self, sdata)
			self.object:set_armor_groups({immortal = 1})
			self.data = sdata and minetest.deserialize(sdata) or {}
		end,
		on_step = function(self, dtime)
			if not self.data or not self.data.watch
			or not self.data.pos or not self.data.node
			then return end
			local pos = sz_pos:new(self.data.pos)
			if pos:node_get().name == self.data.watch then return end
			pos:node_set(self.data.node, self.data.meta)
			minetest.after(0, function() pos:node_set(self.data.node,
						self.data.meta) end)
				self.object:remove()
			end
		})

-- Helper function to try to move a slime to a location, returns true
-- if the slime's movement has been handled.
	local function trymoveslime(pos, try, def)
		-- New location is acceptable only if it's either air, or
		-- a flowing liquid (e.g. the slime's own slime), or
		-- buildable_to, or specifically permeable to slimes, though
		-- they can only tunnel horizontally (to keep them from digging
		-- down to hide from the sun).
		local n = try:node_get()
		if not n or n.name == "ignore" then return end
		local d = try:nodedef()
		if n.name ~= "air" and d.liquidtype ~= "flowing"
		and not d.buildable_to
		and not (d.groups and (d.groups.sz_slime_soluble
				or (d.groups.sz_slime_permeable and (d.groups.sz_slime_permeable <= 1
						or math_random() < (1 / d.groups.sz_slime_permeable))
					and pos.y == try.y)))
		then return end

		-- Slimes are repelled by sunlight, and killed by strong sunlight.
		local nowlight = try:light()
		if try:light(0) < nowlight then
			if nowlight >= 13 then
				pos:smoke(20)
				pos:node_set({
						name = def.liquid_alternative_flowing,
						param2 = 7
					})
				return true
			elseif nowlight > 8 then
				return
			end
		end

		-- The new location is acceptable only if it's adjacent
		-- to a solid surface (including diagonals, or the slime would
		-- not be able to navigate outside corners).
		if not try:scan_around(1, function(n)
				local d = n:nodedef()
				return d and d.walkable
			end) then return end

		-- Move the node and its metadata to the new location.
		-- If the node we're about to replace is a tangible one that we're
		-- to "engulf", instead of simply replace, then we need to set a
		-- "guard" entity there.
		local ent
		if not d.buildable_to and not d.sz_slime_soluble then
			ent = try:entity_add(entname):get_luaentity()
			local n, m = try:node_get(true)
			ent.data = {pos = try, node = n, meta = m}
		end
		pos:node_moveto(try, {
				name = def.liquid_alternative_flowing,
				param2 = 7
			})
		if ent then ent.data.watch = def.liquid_alternative_source end
		return true
	end

-- Register ABM for slime movement.
	minetest.register_abm({
			nodenames = { "group:sz_slime_nucleus" },
			interval = 1,
			chance = 1,
			action = function(pos, node)
				pos = sz_pos:new(pos)

				-- Figure out what type of slime is at this location.
				local def = minetest.registered_nodes[node.name]
				if not def or not def.sz_slime_name then
					return
				end

				-- Check metadata and see if we're in a "sleep" cycle.
				-- At the end of the sleep cycle, start a random-length
				-- "move" cycle.
				local meta = pos:meta()
				local sleep = meta:get_int("sleep")
				if sleep and sleep > 0 then
					sleep = sleep - 1
					meta:set_int("sleep", sleep)
					if sleep < 1 then
						meta:set_int("move", math_random(5, 9))
					end
					return
				end

				-- Check metadata for "move" cycle, and initiate a
				-- "sleep" cycle at its end.
				local move = meta:get_int("move")
				if move and move > 0 then
					move = move - 1
					meta:set_int("move", move)
				end
				if not move or move < 1 then
					meta:set_int("sleep", math_random(10, 30))
					return
				end

				-- Slimes now hunt towards a player, if there is a clear
				-- direction in which to move.
				if math_random() <= 0.5 then
					local npos, nlen
					for k, v in pairs(minetest.get_connected_players()) do
						local p = pos:sub(v:getpos()):neg()
						local l = p:len()
						if l < 32 and (not nlen or l < nlen) then
							nlen = l
							npos = p
						end
					end
					-- Slimes hate to be below their target (they want
					-- to spread out and engulf) so they'll try to move
					-- upwards preferentially.
					if npos then
						if npos.y > 0 and trymoveslime(pos,
							sz_pos.dirs.u, def) then return end
						if trymoveslime(pos, pos:add(npos:dir()), def) then return end
					end
				end

				-- Check each of the 6 directions, in random order, for a new
				-- location to which to move the slime nucleus.
				for k, v in ipairs(sz_pos.shuffledirs()) do
					if trymoveslime(pos, pos:add(v), def) then return end
				end
			end,
		})

------------------------------------------------------------------------
-- SLIME SPAWNING

-- Loop to spawn slimes.  ABM's are too expensive for this, since slimes
-- spawn against very common nodes.  Doing this in our own timer allows
-- us to control the check rate independent of number of nodes of
-- eligible type in range.
	local maxrange = 128
	local minrange = 16 * 16
	local slimespawn = function()
		-- If no players connected, skip.	
		local players = minetest.get_connected_players()
		if not players or #players < 1 then return end

		-- Pick a random point in the area around a random player.
		local player = players[math_random(1, #players)]	
		local pos = sz_pos:new(player:getpos()):add(sz_pos.zero
			:scatter():scale(maxrange)):round()

		-- Make sure the point is air, and dark enough.  This means it
		-- must be loaded, and cannot be occupied.
		if pos:node_get().name ~= "air" or pos:light() > 7 then
			return
		end

		-- Make sure this point does not have any players too close.  It
		-- would be unfair to spawn a slime right next to a player, and
		-- we don't want the mechanics of slime spawning to be too
		-- closely visible.
		for k, v in pairs(players) do
			local d = pos:sub(v:getpos())
			if d:dot(d) < minrange then
				return
			end
		end

		-- There needs to be a solid surface near the spawn spot;
		-- slimes can only climb floors/walls/ceilings, they cannot fly.
		if not pos:scan_around(1, function(n)
				local d = n:nodedef()
				return d and d.walkable
			end) then return end

		-- Slimes are no longer allowed to spawn against trees, as it's
		-- way too annoying to light those above-ground.
		if pos:scan_around(1, function(n)
				return n:groups().leaves
			end) then return end

		-- Make sure there aren't already a bunch of slimes nearby.
		-- Search within 1 mapblock in each direction.
		local min = {
			x = (math_floor(pos.x / 16) - 2) * 16,
			y = (math_floor(pos.y / 16) - 2) * 16,
			z = (math_floor(pos.z / 16) - 2) * 16,
		}
		local max = {
			x = (math_floor(pos.x / 16) + 2) * 16 + 15,
			y = (math_floor(pos.y / 16) + 2) * 16 + 15,
			z = (math_floor(pos.z / 16) + 2) * 16 + 15,
		}
		for k, v in pairs(minetest.find_nodes_in_area(min, max,
				{ "group:sz_slime" })) do
			return
		end

		-- Scan all nearby nodes for ones that influence the type of
		-- slime to spawn.
		local total = 0
		local probs = sz_table:new()
		pos:scan_flood(16, function(p)
				local n = p:node_get().name
				local t = slimespawn[n]
				if t then
					probs[t] = (probs[t] or 0) + 1
					total = total + 1
				end
				if n ~= "air" then return false end
			end)
		if total < 1 then return end

		-- Pick a type of slime randomly, weighted by the spawn
		-- probabilities gathered from nearby nodes.
		local pick = math_random(1, total)
		local picked = nil 
		for k, v in pairs(probs) do
			pick = pick - v
			if pick <= 0 then
				picked = k
				break
			end
		end

		pos:node_set({ name = picked })
		print(modname .. ": spawned " .. picked .. " at " .. tostring(pos))
	end

	local cycles = 0
	minetest.register_globalstep(function(t)
			cycles = cycles + t
			while cycles >= 1 do
				slimespawn()
				cycles = cycles - 1
			end
		end)
