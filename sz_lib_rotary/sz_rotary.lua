-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_pos, sz_rotary, sz_table, type
    = minetest, pairs, sz_pos, sz_rotary, sz_table, type
-- LUALOCALS > ---------------------------------------------------------

-- Register a node for rotary power use.  This adds some automatic hooks
-- to handle rotary power state checks, and makes sure that the node
-- is in the sz_rotary group, for abm purposes.
local function chain(m1, m2)
	return function(...)
		if m1 and not m2 then return m1(...) end
		if m1 then m1(...) end
		if m2 then return m2(...) end
	end
end
function sz_rotary.register_node(name, def)
	-- Trigger rotary a rotary power update for the new node.
	def.on_construct = chain(def.on_construct, function(pos)
			minetest.after(0, function()
					sz_pos:new(pos):rotary_power_check()
				end)
		end)

	-- After destroying a rotary-involved node, trigger power
	-- checks around it, in case it was supplying/transmitting
	-- power to neighbors.
	def.after_destruct = chain(def.after_destruct, function(pos)
			-- Metadata will already have been cleared automatically, so we
			-- just need to send out power notifications.
			pos = sz_pos:new(pos)
			for k, v in pairs(sz_pos.dirs) do
				pos:add(v):rotary_power_check()
			end
		end)

	-- Add the sz_rotary group (for the power check abm) and allow
	-- these nodes to shatter under certain circumstances.
	def.groups = sz_table.merge(def.groups or {}, {
			sz_rotary = 1,
			can_shatter = 1
		})

	minetest.register_node(name, def)
	def = minetest.registered_nodes[name]
	return def
end

-- Register a node to operate on rotary power, and also apply facedir.
-- "Standard" facedir only rotates a node on the x/z plane based on
-- facing.  This modified form makes placing nodes rotated onto the
-- y axis easier, if the direction the placer is facing is predominantly
-- along the y axis.
function sz_rotary.register_node_facedir(name, def)
	return sz_rotary.register_node(name, sz_table.merge(def, {
				paramtype2 = "facedir",
				on_place = minetest.rotate_node
			}))
end

-- Helper to create a "standard" animation, defined as vertical
-- frames of uniform size.
function sz_rotary.std_anim(name, length, size, type)
	length = length or 1
	size = size or 16
	type = type or "vertical_frames"
	return {
		name = name,
		animation = {
			type = type,
			aspect_w = size,
			aspect_h = size,
			length = length
		}
	}
end
