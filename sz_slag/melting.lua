-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_pos
    = minetest, pairs, sz_pos
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- Extension to sz_pos to melt a node.
function sz_pos:slagmelt(items)
	if not items then
		local def = self:nodedef()
		if def then items = def.slagdrops end
	end
	self:node_set({ name = modname .. ":slag_source" })
	if not items then return end
	local inv = self:meta():get_inventory()
	for k, v in pairs(items) do
		inv:add_item("main", k .. " " .. v)
	end
end

-- ABM to check for node melting automatically.
minetest.register_abm({
		nodenames = { "group:slagmelt" },
		interval = 2,
		chance = 5,
		action = function(pos)
			pos = sz_pos:new(pos)
			if pos:groups().slagmelt <= pos:heat_level() then
				pos:slagmelt()
			end
		end
	})
