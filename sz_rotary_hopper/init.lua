-- LUALOCALS < ---------------------------------------------------------
local ItemStack, default, minetest, pairs, sz_pos, sz_rotary
    = ItemStack, default, minetest, pairs, sz_pos, sz_rotary
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local hopp_pw_name = modname .. ":hopper_powered"
local hopp_nopw_name = modname .. ":hopper"

------------------------------------------------------------------------
-- POWER CHECK

local function hopp_power_check(pos, node)
	pos = sz_pos:new(pos)
	local pw, ign
	for k, v in pairs({ sz_pos.dirs.n, sz_pos.dirs.s, sz_pos.dirs.e, sz_pos.dirs.w }) do
		pw, ign = pos:rotpower_in(v)
		if ign then return end
		if pw then break end
	end
	if not pw then
		if node.name ~= hopp_nopw_name then
			pos:node_set({ name = hopp_nopw_name })
		end
		return
	else
		if node.name ~= hopp_pw_name then
			pos:node_set({ name = hopp_pw_name })
		end
	end
end

------------------------------------------------------------------------
-- HOPPER NODE

local function reg_hopp(name, def)
	def.description = def.description or "Hopper"
	def.drop = def.drop or hopp_nopw_name
	def.groups = def.groups or { }
	def.groups.choppy = def.groups.choppy or 2
	def.groups.oddly_breakable_by_hand = def.groups.oddly_breakable_by_hand or 1
	def.groups.sz_hopper = def.groups.sz_hopper or 1
	def.groups.flammable = def.groups.flammable or 1
	def.groups.saw_delicate = def.groups.saw_delicate or 1
	def.rotary_power_check = def.rotary_power_check or hopp_power_check
	def.sounds = def.sounds or default.node_sound_wood_defaults()
	return sz_rotary.register_node(name, def)
end

reg_hopp(hopp_nopw_name, {
	tiles = {
		"sz_rotary_hopper_top_anim.png^[verticalframe:16:1",
		"sz_rotary_hopper_top_anim.png^[transformR180^[verticalframe:16:1",
		"sz_lib_rotary_woodbox_input.png"
	}
})
reg_hopp(hopp_pw_name, {
	tiles = {
		sz_rotary.std_anim("sz_rotary_hopper_top_anim.png"),
		sz_rotary.std_anim("sz_rotary_hopper_top_anim.png^[transformR180"),
		"sz_lib_rotary_woodbox_input.png"
	}
})

do
	local gear = "sz_lib_rotary:gear_wood"
	local corb = "group:mininode_corbel_wood"
	minetest.register_craft({
		output = hopp_nopw_name,
		recipe = {
			{ corb, gear, corb },
			{ gear, "",   gear },
			{ corb, gear, corb },
		}
	})
end

------------------------------------------------------------------------
-- HOPPER ITEM MOVEMENT ABM

local function getconsume(below)
	local node = below:node_get()
	if node.name == "ignore" then return end

	-- Check for hopper API first.
	local def = below:nodedef()
	if def.on_item_inject then
		return function(stack)
			return def.on_item_inject(below, node, stack)
		end
	end

	-- Eject items to empty space
	if below:is_empty() then
		return function(stack)
			below:item_eject(stack)
			return ""
		end
	end

	-- Inject items into inventories
	local inv = below:meta():get_inventory()
	if inv then
		for k, v in pairs({ "main", "src" }) do
			local size = inv:get_size(v)
			if size and size > 0 then
				return function(stack)
					return inv:add_item(v, stack)
				end
			end
		end
	end
end

minetest.register_abm({
	nodenames = { hopp_pw_name },
	interval = 1,
	chance = 1,
	action = function(pos)
		pos = sz_pos:new(pos)

		local below = pos:add(sz_pos.dirs.d)
		local consume = getconsume(below)
		if not consume then return end

		-- Consume loose items in entities.
		local above = pos:add(sz_pos.dirs.u)
		for k, v in pairs(above:objects_in_radius(2)) do
			if not v:is_player() and sz_pos.round(v:getpos()):eq(above) then
				local ent = v:get_luaentity()
				if ent and ent.itemstring then
					local stack = ItemStack(consume(ItemStack(ent.itemstring)))
					if stack:is_empty() then
						v:remove()
					else
						ent.itemstring = stack:to_string()
					end
				end
			end
		end

		-- Consume from items_on_ground (i.e. stuffpile) nodes.
		if above:groups().items_on_ground then
			local inv = above:meta():get_inventory()
			local t = inv:get_list("main")
			for i, v in pairs(t) do
				t[i] = consume(v)
			end
			inv:set_list("main", t)
		end
	end
})
