-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_pos, sz_util, tonumber
    = minetest, pairs, sz_pos, sz_util, tonumber
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
sz_util.modrenamed(modname, "sz_broom")

minetest.register_tool(modname .. ":broom", {
		description = "Broom",
		inventory_image = "sz_tool_broom.png",
		on_use = function(itemstack, user, pointed_thing)
			if not pointed_thing then return end
			local pos = pointed_thing.above or pointed_thing.under
			if not pos and pointed_thing.ref then
				pos = sz_pos.round(pointed_thing.ref:getpos())
			end
			if not pos then return end
			pos = sz_pos:new(pos)

			local radius = sz_pos:xyz(3.5, 1.5, 3.5)
			local minpos = pos:add(radius:neg())
			local maxpos = pos:add(radius)

			local wear = 65

			for i, v in pairs(minetest.find_nodes_in_area(
					minpos, maxpos, { "group:items_on_ground" })) do
				wear = wear + 5
				v = sz_pos:new(v)
				local punch = v:nodedef().on_punch
				if punch then
					punch(v, v:node_get(), user)
				else
					minetest.node_punch(v, v:node_get(), user)
				end
			end

			for i, v in pairs(minetest.get_objects_inside_radius(pos, 4)) do
				if not v:is_player() then
					wear = wear + 1
					local rpos = sz_pos:new(v:getpos()):round()
					if rpos.x > minpos.x and rpos.x < maxpos.x
					and rpos.y > minpos.y and rpos.y < maxpos.y
					and rpos.z > minpos.z and rpos.z < maxpos.z then
						v:punch(user, 1, { })
					end
				end
			end

			wear = wear + tonumber(itemstack:get_wear())
			if wear > 65535 then
				itemstack:clear()
				pos:sound("default_tool_breaks", {gain = 0.5})
				return itemstack
			end
			itemstack:set_wear(wear)
			return itemstack
		end,
	})

minetest.register_craft({
		output = modname .. ":broom",
		recipe = {
			{ "farming:wheat" },
			{ "group:stick" },
			{ "group:stick" }
		},
	})
