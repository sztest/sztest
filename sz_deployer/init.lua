-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, minetest, sz_pos, sz_util, type
    = default, ipairs, minetest, sz_pos, sz_util, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
sz_util.modrenamed(modname, "sz_rotary_deployer")

local function deploy(pos, node, stack)
	pos = sz_pos:new(pos)
	for k, v in ipairs(sz_pos.shuffledirs()) do
		local targ = pos:add(v)
		local out, succ = minetest.item_place_node(stack, nil,
			{ type = "node", above = pos, under = targ })
		if succ then return out end
	end
	return stack
end

minetest.register_node(modname .. ":deployer", {
	description = "Deployer",
	tiles = { "default_chest_top.png^[transformR90" },
	groups = {
		wood = 1,
		choppy = 3,
		oddly_breakable_by_hand = 3
	},
	sounds = default.node_sound_wood_defaults(),
	on_item_inject = deploy
})

do
	local corb = "group:mininode_corbel_wood"
	local gear = "sz_lib_rotary:gear_wood"
	minetest.register_craft({
		output = modname .. ":deployer",
		recipe = {
			{ corb, "", corb },
			{ "", gear, "" },
			{ corb, "", corb },
		}
	})
end
