-- LUALOCALS < ---------------------------------------------------------
local io, ipairs, minetest, pairs, sz_pos, sz_table
    = io, ipairs, minetest, pairs, sz_pos, sz_table
local io_close, io_open
    = io.close, io.open
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

core.register_privilege("nodedebug", "Can enable node debugging info")

------------------------------------------------------------------------
-- MANAGE SELECTED NODES TO DEBUG

local datapath = minetest.get_worldpath() .. "/" .. modname .. ".txt"

local selnodes = { }
do
	local f = io_open(datapath, "rb")
	if f then
		local d = f:read("*all")
		io_close(f)
		selnodes = minetest.deserialize(d) or { }
	end
end

minetest.register_chatcommand("nodedebug", {
		privs = {nodedebug = true},
		description = "Choose which nodes to debug.",
		func = function(name, param)
			if param and param ~= "" then
				if param == ":" then param = nil end
				selnodes[name] = param
				local f = io_open(datapath, "wb")
				if f then
					f:write(minetest.serialize(selnodes))
					io_close(f)
				end
			else
				param = selnodes[name]
			end
			local function say(t) minetest.chat_send_player(name, t) end
			if param then
				say("Debugging \"" .. param .. "\" (\"/nodedebug :\" to disable)")
			else
				say("Not debugging any nodes.")
			end
		end})

------------------------------------------------------------------------
-- UPDATE DEBUG HUDS CONTINUOUSLY

local function debugdata(pos)
	local node = pos:node_get()
	local name = node.name .. ":" .. node.param1 .. ":" .. node.param2

	local lines = sz_table:new()
	lines:insert(" @ " .. pos:to_string())

	local environ = sz_table:new()
	environ:insert("light: " .. pos:light(0)
		.. "/" .. pos:light()
		.. "/" .. pos:light(0.5))
	local heat = pos:heat_level()
	if heat ~= 0 then environ:insert("heat: " .. heat) end
	lines:insert(environ:concat(", "))

	local mf = sz_table:new(pos:meta():to_table().fields)
	local keys = mf:keys()
	keys:sort()
	for i, k in ipairs(keys) do
		local s = mf[k]
		if #s > 50 then s = s:sub(0, 47) .. "..." end
		lines:insert(k .. " = " .. s)
	end

	return name, lines:concat("\n")
end

local allhuds = {}

minetest.register_globalstep(function()
		for k, p in pairs(allhuds) do
			for k, h in pairs(p) do
				h(true)
			end
		end
	end)

------------------------------------------------------------------------
-- PERIODICALLY SCAN NODES AND ATTACH HUDS

local function mkhud(player, pos)
	local name, text = debugdata(pos)
	local id = player:hud_add({
			hud_elem_type = "waypoint",
			world_pos = pos,
			name = name,
			text = text,
			number = 0xFFFF80
		})
	return function(keep)
		if not keep then return player:hud_remove(id) end
		local n, t = debugdata(pos)
		if n ~= name then player:hud_change(id, "name", n) end
		name = n
		if t ~= text then player:hud_change(id, "text", t) end
		text = t
	end
end

local function rescan()
	minetest.after(1, rescan)
	for i, player in ipairs(minetest.get_connected_players()) do
		local n = player:get_player_name()
		local phuds = allhuds[n] or {}
		if not minetest.get_player_privs(player:get_player_name()).nodedebug then
			for k, v in pairs(phuds) do
				v()
			end
			allhuds[n] = nil
		else
			local newhuds = {}
			local sel = selnodes[n]
			if sel then
				local bydist = sz_table:new()
				local ppos = sz_pos:new(player:getpos())
				for i, pos in ipairs(ppos:nodes_in_area(16, sel)) do
					pos = sz_pos:new(pos)
					local d = pos:sub(ppos)
					d = d:dot(d)
					bydist[d] = bydist[d] or { }
					bydist[d][pos:hash()] = pos
				end
				local dists = bydist:keys()
				dists:sort()
				local t = 0
				for i, d in ipairs(dists) do
					for hash, pos in pairs(bydist[d]) do
						t = t + 1
						if t > 20 then break end
						local hud = phuds[hash] or mkhud(player, pos)
						newhuds[hash] = hud
						phuds[hash] = nil
						hud(true)
					end
					if t > 20 then break end
				end
			end
			for k, v in pairs(phuds) do
				v()
			end
			allhuds[n] = newhuds
		end
	end
end
minetest.after(1, rescan)
