-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, minetest, pairs, sz_facedir, sz_pos, sz_rotary,
      sz_table, type
    = default, ipairs, minetest, pairs, sz_facedir, sz_pos, sz_rotary,
      sz_table, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function reg_trans(name, def)
	return sz_rotary.register_node_facedir(name, sz_table.mergedeep(def or {}, {
				groups = {choppy = 2, oddly_breakable_by_hand = 1, flammable = 1},
				sounds = default.node_sound_wood_defaults()
			}))
end

------------------------------------------------------------------------
-- AXLES

local axle_nopw_name = modname .. ":axle"
local axle_pw_name = modname .. ":axle_powered"

local function axle_power_check(pos, node)
	pos = sz_pos:new(pos)
	local facedir = sz_facedir:from_param(node.param2)
	local pw, pwdir
	for k, v in ipairs({ facedir:top(), facedir:bottom() }) do
		local np, ign = pos:rotpower_in(v, true)
		if ign then return end
		if np then
			pw = np
			if pwdir ~= nil then
				pos:shatter("axle power conflict")
				return
			end
			pwdir = v:neg()
		end
	end
	if pw then
		local nf = pwdir:neg():top_to_facedir().param
		if node.name ~= axle_pw_name or node.param2 ~= nf then
			pos:node_set({
					name = axle_pw_name,
					param = node.param,
					param2 = nf
				})
		end
		local po = pos:rotpower_out():clear()
		po.qty = pw.qty - 1
		po.axle = true
		po:dir_set(pwdir, true)
		po:save()
	else
		if node.name ~= axle_nopw_name then
			pos:node_set({
					name = axle_nopw_name,
					param = node.param,
					param2 = node.param2
				})
		end
		pos:rotpower_out():clear():save()
	end
end

local function reg_axle(name, def)
	return reg_trans(name, sz_table.mergedeep(def or {}, {
				description = "Axle",
				drop = axle_nopw_name,
				drawtype = "nodebox",
				node_box = {
					type = "fixed",
					fixed = {
						{ -0.1, -0.5, -0.1, 0.1, 0.5, 0.1 }
					}
				},
				paramtype = "light",
				rotary_power_check = axle_power_check,
				groups = {sz_axle = 1},
				sunlight_propagates = true
			}))
end

reg_axle(axle_nopw_name, {tiles = {"default_wood.png"}})
reg_axle(axle_pw_name, {
		tiles = {
			"default_wood.png",
			"default_wood.png",
			sz_rotary.std_anim("sz_rotary_transmit_wood_anim.png")
		},
		damage_per_second = 1,
		groups = {not_in_creative_inventory = 1}
	})

------------------------------------------------------------------------
-- GEARBOXES

local gb_nopw_name = modname .. ":gearbox"
local gb_pw_name = modname .. ":gearbox_powered"

-- Gearboxes can be disabled by other nodes by simply replacing
-- the gearbox node with its "disabled" version.
local gb_disable_name = modname .. ":gearbox_disabled"

local function gb_power_check(pos, node)
	pos = sz_pos:new(pos)
	minetest.after(0.25, function()
			if not pos:groups().sz_gearbox then return end

			node = pos:node_get()
			if node.name == gb_disable_name then
				pos:rotpower_out():clear():save()
				return
			end

			local indir = sz_facedir:from_param(node.param2):bottom()
			local pw, ign = pos:rotpower_in(indir)
			if ign then return end

			if pw then
				if node.name ~= gb_pw_name then
					pos:node_set({
							name = gb_pw_name,
							param = node.param,
							param2 = node.param2
						})
				end
				local po = pos:rotpower_out()
				for k, v in pairs(sz_pos.dirs) do
					po:dir_set(v, not v:eq(indir))
				end
				po.qty = 16
				po.axle = false
				po:save()
			else
				if(node.name ~= gb_nopw_name) then
					pos:node_set({
							name = gb_nopw_name,
							param = node.param,
							param2 = node.param2
						})
				end
				pos:rotpower_out():clear():save()
			end
		end)
end

local function reg_gb(name, def)
	return reg_trans(name, sz_table.mergedeep(def or {}, {
				description = "Gearbox",
				drop = gb_nopw_name,
				rotary_power_check = gb_power_check,
				groups = {sz_gearbox = 1, sz_saw_delicate = 1}
			}))
end

local function reg_gb_unpw(name, def)
	reg_gb(name, sz_table.mergedeep(def or {}, {
				tiles = {
					"sz_lib_rotary_woodbox_output.png",
					"sz_lib_rotary_woodbox_input.png",
					"sz_lib_rotary_woodbox_output.png"
				},
			}))
end
reg_gb_unpw(gb_nopw_name)
reg_gb_unpw(gb_disable_name, {groups = {not_in_creative_inventory = 1}})

local gb_anim = sz_rotary.std_anim("sz_lib_rotary_woodbox_output_anim.png")
reg_gb(gb_pw_name, {
		tiles = {
			gb_anim,
			"sz_lib_rotary_woodbox_input.png",
			gb_anim
		},
		groups = {
			not_in_creative_inventory = 1,
			sz_rotary_ambiance = 1
		}
	})


------------------------------------------------------------------------
-- NODE RECIPES

minetest.register_craft({
		output = axle_nopw_name,
		recipe = {
			{ "sz_lib_rotary:gear_wood", "group:stick", "sz_lib_rotary:gear_wood" },
		}
	})
minetest.register_craft({
		output = axle_nopw_name,
		recipe = {
			{"sz_lib_rotary:gear_wood"},
			{"group:stick"}, 
			{"sz_lib_rotary:gear_wood"},
		}
	})

minetest.register_craft({
		output = gb_nopw_name,
		recipe = {
			{ "group:wood", "sz_lib_rotary:gear_wood", "group:wood" },
			{ "sz_lib_rotary:gear_wood", "sz_lib_rotary:gear_wood",
				"sz_lib_rotary:gear_wood" },
			{ "group:wood", "sz_lib_rotary:gear_wood", "group:wood" },
		}
	})
