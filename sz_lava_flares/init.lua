-- LUALOCALS < ---------------------------------------------------------
local math, minetest, sz_pos
    = math, minetest, sz_pos
local math_floor, math_log, math_random
    = math.floor, math.log, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local exporand
exporand = function()
	local x = math_random()
	if x == 0 then return exporand() end
	return -math_log(x)
end

local entname = modname .. ":flare"

minetest.register_entity(entname, {
		initial_properties = {
			hp_max = 1,
			physical = true,
			collide_with_objects = false,
			collisionbox = { 0, 0, 0, 0, 0, 0 },
			is_visible = false,
		},
		get_staticdata = function(self)
			self.data = self.data or {}
			return minetest.serialize(self.data)
		end,
		on_activate = function(self, sdata)
			self.object:set_armor_groups({ immortal = 1})
			self.data = sdata and minetest.deserialize(sdata) or {}
			if self.data.init then return end
			self.data.init = true
			self.data.pos = self.data.pos or self.object:getpos()
			self.object:setvelocity(sz_pos.dirs.u:scatter(1)
				:scale(exporand() * 10))
			self.object:setacceleration(sz_pos.dirs.d:scale(10))
		end,
		on_step = function(self, dtime)
			local pos = sz_pos:new(self.object:getpos())

			if #(pos:nodes_in_area(1, {"ignore"})) > 0 then
				self.object:remove()
				return
			end

			local oldpos = self.data.pos or pos
			self.data.pos = pos

			local rel = pos:neg():add(oldpos)
			local dir = rel:norm()
			for r = 0, math_floor(rel:len()) do
				local p = pos:add(dir:scale(r))
				if p:node_get().name == "air" then
					p:node_set({name = "fire:basic_flame"})
				end
			end

			if pos:nodedef().walkable then
				self.object:remove()
			end
		end
	})

minetest.register_abm({
		nodenames = { "group:lava" },
		neighbors = { "air" },
		interval = 20,
		chance = 1000,
		action = function(pos)
			pos = sz_pos:new(pos)
			minetest.add_entity(pos, entname)
		end
	})
