
Wind turbine:
- Better name?  Shore Wind Turbine?  Sea Breeze Turbine?
- Needs to jam on actors, and probably damage them if they get in the
  way of the (currently imaginary) vanes.
- If jammed while running, should probably shatter.
- Add a visual for the vanes; maybe an entity that's a flat billboard?
  Maybe we can do it with nodebox?  Maybe a sister node with another
  drawtype?
- Allow certain small things in path, like torches, without disrupting
  airflow.
