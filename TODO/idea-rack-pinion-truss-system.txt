RACK AND PINION DRIVE SYSTEM
A "truss" node that's capable of being moved by being pushed/pulled by
conneced trusses; a "rack truss" node that has a "rack" gear attached
to it; a "pinion" node that moves adjacent rack truss nodes laterally
when rotated by a rotor.  Need to shatter pinion nodes if more than one
are fighting over the direction to move a series of connected trusses.
Trusses could push up to a certain total number of other nodes, to
effect a piston equivalent.

