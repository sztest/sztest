-- LUALOCALS < ---------------------------------------------------------
local math, minetest, pairs, sz_pos, sz_stuffpile
    = math, minetest, pairs, sz_pos, sz_stuffpile
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- LOOSE ITEMS PUSHED BY WATER-FLOWS

local bitemname = "__builtin:item"
local bitem = minetest.registered_entities[bitemname]

local old_step = bitem.on_step
bitem.on_step = function(self, dtime, ...)
	old_step(self, dtime, ...)

	local pos = sz_pos:new(self.object:getpos())
	local posdef = pos:nodedef()

	-- Items sitting in flowing fluids atop a solid node will be
	-- pushed by currents.
	local below = pos:add(sz_pos.dirs.d:scale(0.25)):nodedef()
	if posdef and posdef.liquidtype and below and below.walkable then
		-- Apparently minetest water does not have an internal sense
		-- of "flowing," only "how deep."  Find adjacent flowing
		-- nodes that are less deep and sum up their net velocity.
		local mydepth = pos:fluid_depth(1)
		if mydepth and mydepth > 0 then
			local net = sz_pos:xyz(0, 0, 0)
			for k, v in pairs({ sz_pos.dirs.n, sz_pos.dirs.s,
					sz_pos.dirs.e, sz_pos.dirs.w }) do
				local p = pos:add(v)
				local d = p:fluid_depth(1)
				if d and d ~= mydepth then
					local s = 0.5 * math_random()
					if mydepth < d then s = -s end
					net = net:add(v:scale(s))
				end
			end

			-- Since we're in a fluid, net velocity is dominated by
			-- motion of the medium.
			net = net:scale(0.5 + math_random() / 2)
			net:add(sz_pos:new(self.object:getvelocity()):scale(0.25))
			self.object:setvelocity(net)

			-- Keep object in a physics-simulated state.
			self.object:setacceleration(sz_pos.dirs.d:scale(10))
			self.physical_state = true
			self.object:set_properties({ physical = true })
		end
	end
end

minetest.register_entity(":"..bitemname, bitem)

------------------------------------------------------------------------
-- STUFF PILE ITEMS WASHED OUT BY FLOWING WATER

if minetest.registered_nodes.sz_stuffpile then
	minetest.register_abm({
			nodenames = { sz_stuffpile.nodename },
			interval = 1,
			chance = 1,
			action = function(pos)
				pos = sz_pos:new(pos)
				if pos:fluid_washout() then
					pos:node_signal("selfdestruct")
				end
			end
		})
end
