-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, math, minetest, os, pairs, sz_pos, sz_rotary,
      sz_table, tonumber, tostring
    = default, ipairs, math, minetest, os, pairs, sz_pos, sz_rotary,
      sz_table, tonumber, tostring
local math_abs, os_time
    = math.abs, os.time
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local ts_pw1_name = modname .. ":turnstile_powered_1"
local ts_pw2_name = modname .. ":turnstile_powered_2"
local ts_nopw_name = modname .. ":turnstile"

local sound_pos = {}
local sound_data = {}
local sound_check_pending
local function check_sounds()
	if not sound_check_pending then
		minetest.after(5, function()
				sound_check_pending = nil
				check_sounds()
			end)
	end
	for k, v in pairs(sound_data) do
		local n = v.pos:node_get()
		if n.name ~= ts_pw1_name and n.name ~= ts_pw2_name then
			if v.id then minetest.sound_stop(v.id) end
			sound_data[k] = nil
		else
			local now = os_time()
			if not v.t or v.t < now - 60 then
				if v.id then minetest.sound_stop(v.id) end
				v.id = minetest.sound_play("sz_rotary_turnstile", {
						gain = 0.25,
						pos = v.pos,
						loop = true
					})
				v.t = now
			end
		end
	end
end
check_sounds()

local function ts_power_check(pos)
	pos = sz_pos:new(pos)

	local rota = pos:meta():get_string("rota")
	rota = rota and tonumber(rota) or 0

	local nn = pos:node_get().name
	if rota >= 0.4 then
		if nn ~= ts_pw2_name then
			pos:node_swap({ name = ts_pw2_name })
			sound_data[tostring(pos)] = sound_data[tostring(pos)] or {pos = pos}
			check_sounds()
		end
		local po = pos:rotpower_out():clear()
		po:dir_set(sz_pos.dirs.u, true)
		po:dir_set(sz_pos.dirs.d, true)
		po.qty = 3
		po:save()
	elseif rota <= -0.4 then
		if nn ~= ts_pw1_name then
			pos:node_swap({ name = ts_pw1_name })
			sound_data[tostring(pos)] = sound_data[tostring(pos)] or {pos = pos}
			check_sounds()
		end
		local po = pos:rotpower_out():clear()
		po:dir_set(sz_pos.dirs.u, true)
		po:dir_set(sz_pos.dirs.d, true)
		po.qty = 3
		po:save()
	else
		if nn ~= ts_nopw_name then
			pos:node_swap({ name = ts_nopw_name })
			check_sounds()
		end
		pos:rotpower_out():clear():save()
	end
end

local function reg_turnstile(name, def)
	return sz_rotary.register_node(name, sz_table.mergedeep(def or {}, {
				description = "Turnstile",
				drop = ts_nopw_name,
				groups = {
					choppy = 1,
					oddly_breakable_by_hand = 1,
					flammable = 1,
					sz_turnstile = 1,
					sz_saw_delicate = 1
				},
				rotary_power_check = ts_power_check,
				sounds = default.node_sound_stone_defaults()
			}))
end

reg_turnstile(ts_nopw_name, {
		tiles = {
			"sz_lib_rotary_woodbox_output.png",
			"sz_lib_rotary_woodbox_output.png",
			"sz_rotary_turnstile_side.png",
		},
	})
reg_turnstile(ts_pw1_name, {
		tiles = {
			sz_rotary.std_anim("sz_lib_rotary_woodbox_output_anim.png"),
			sz_rotary.std_anim("sz_lib_rotary_woodbox_output_anim.png"),
			sz_rotary.std_anim("sz_rotary_turnstile_side_anim.png"),
		},
		groups = {not_in_creative_inventory = 1, sz_rotary_ambiance = 1}
	})
reg_turnstile(ts_pw2_name, {
		tiles = {
			sz_rotary.std_anim("sz_lib_rotary_woodbox_output_anim.png"),
			sz_rotary.std_anim("sz_lib_rotary_woodbox_output_anim.png"),
			sz_rotary.std_anim("sz_rotary_turnstile_side_anim.png^[transformFX"),
		},
		groups = {not_in_creative_inventory = 1, sz_rotary_ambiance = 1}
	})

minetest.register_abm({
		nodenames = { "group:sz_turnstile" },
		interval = 1,
		chance = 1,
		action = function(pos, node)
			pos = sz_pos:new(pos)
			local meta = pos:meta()

			local oldents = meta:get_string("ents")
			oldents = oldents and oldents ~= ""
			and minetest.deserialize(oldents) or {}

			local newents = sz_table:new()
			local rota = 0
			local gotplayer
			for i, v in ipairs(pos:tangible_in_radius(5)) do
				if v and v:is_player() then
					gotplayer = true
					local pn = v:get_player_name()
					local pp = sz_pos:new(v:getpos()):sub(pos):norm()
					newents[pn] = pp
					if oldents[pn] then
						local op = sz_pos:new(oldents[pn])
						rota = rota + op:cross(pp:sub(op)).y
					end
				end
			end
			meta:set_string("ents", newents:serialize())

			local oldrota = meta:get_string("rota")
			oldrota = oldrota and tonumber(oldrota) or 0
			rota = (oldrota * 7 + rota) / 8

			if math_abs(rota) < 0.4 and not gotplayer
			or math_abs(rota) < 0.05 then
				rota = 0
			end

			if rota ~= oldrota then 
				meta:set_string("rota", rota)
				ts_power_check(pos)
			end
		end
	})

minetest.register_craft({
		output = ts_nopw_name,
		recipe = {
			{ "", "group:stick", "" },
			{ "group:stick", "sz_rotary_transmit:gearbox", "group:stick" },
			{ "", "group:stick", "" },
		}
	})
