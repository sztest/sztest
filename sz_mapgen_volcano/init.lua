-- LUALOCALS < ---------------------------------------------------------
local VoxelArea, default, math, minetest, pairs, sz_pos
    = VoxelArea, default, math, minetest, pairs, sz_pos
local math_ceil, math_exp, math_floor, math_random
    = math.ceil, math.exp, math.floor, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local MAPSIZE = 65536
local CELSIZE = 1024
local MAPCELS = MAPSIZE / CELSIZE
local CELOFFS = MAPCELS / 2

local YSTEP = 32
local YCELS = MAPSIZE / YSTEP
local YOFFS = YCELS / 2
local RMAX = 128
local RMIN = 3

local SLOPE = 1 / 3
local SPRING = 1 / 16

------------------------------------------------------------------------
-- X/Z LOCATION DATA

local confroot

local function initconfig()
	initconfig = function() end

	local params = {
		scale = 1,
		spread = { x = 32, y = 32, z = 32 },
		octaves = 4,
		persist = 0.75
	}
	local size = { x = MAPCELS, y = MAPCELS }
	local perlmap = minetest.get_perlin_map(params, size)

	local perlx = perlmap:get2dMap_flat({x = 0, y = 0, z = 0})
	local perlz = perlmap:get2dMap_flat({x = 0, y = 1024, z = 0})
	local perly = perlmap:get2dMap_flat({x = 0, y = 2048, z = 0})

	confroot = {}
	local LASTCEL = MAPCELS - 1
	for z = 0, LASTCEL do
		local row = {}
		confroot[#confroot + 1] = row
		for x = 0, LASTCEL do
			local idx = z * MAPCELS + x + 1
			row[#row + 1] = {
				i = idx,
				x = math_floor(((x - CELOFFS) + perlx[idx]) * CELSIZE),
				z = math_floor(((z - CELOFFS) + perlz[idx]) * CELSIZE)
			}
		end
	end
end

------------------------------------------------------------------------
-- DEPTH/RADIUS DATA

local confdepth = { }

local function loaddepth(cel)
	local data = confdepth[cel.i]
	if data then return data end

	local params = {
		scale = 1,
		spread = { x = 32, y = 32, z = 32 },
		octaves = 4,
		persist = 0.75
	}
	local size = { x = 1, y = YCELS }
	local perlmap = minetest.get_perlin_map(params, size)
	local perly = perlmap:get2dMap_flat({x = 0, y = 0, z = 0})

	data = { }
	local r = RMAX / 2
	for y = 1, YCELS do
		r = r * math_exp(perly[y] * SLOPE) * (1 - SPRING) + RMAX / 2 * SPRING
		if r > RMAX then r = RMAX * 2 - r end
		if r < RMIN then r = RMIN * 2 - r end
		data[#data + 1] = math_floor(r)
	end

	confdepth[cel.i] = data
	return data
end

------------------------------------------------------------------------
-- LAVA NODE DEFINITIONS AND ABM

local srcname = modname .. ":volcano_lava_source"
local flowname = modname .. ":volcano_lava_flowing"
local function defmerge(t)
	local ref = minetest.registered_nodes["default:lava_" .. t]
	local def = { liquid_alternative_source = srcname,
		liquid_alternative_flowing = flowname,
		groups = { lava_volcano = 1, lava = 1, liquid = 2, igniter = 1 }
	}
	for k, v in pairs(ref) do
		if not def[k] then def[k] = v end
	end
	if t == "flowing" then def.groups.lava_flow = 1 end
	minetest.register_node(modname .. ":volcano_lava_" .. t, def)
end
defmerge("source")
defmerge("flowing")

if minetest.settings:get_bool("enable_lavacooling") ~= false then
	minetest.register_abm({
			label = "Lava cooling (volcanic)",
			nodenames = {"group:lava_volcano"},
			neighbors = {"group:cools_lava", "group:water"},
			interval = 2,
			chance = 2,
			catch_up = false,
			action = function(...)
				local old = minetest.set_node
				minetest.set_node = function(pos, node, ...)
					if sz_pos:new(pos):node_get().name == srcname
					and -math_random(0, 1024) > pos.y then
						node.name = "default:obsidian"
					end
					return old(pos, node, ...)
				end
				default.cool_lava(...)
				minetest.set_node = old
			end,
		})
end

------------------------------------------------------------------------
-- MAP GENERATION HOOK

local airid = minetest.get_content_id("air")
local srcid = minetest.get_content_id(srcname)

minetest.register_on_generated(function(minp, maxp, seed)
		initconfig()

		local minxcel = math_floor(minp.x / CELSIZE + CELOFFS) - 2
		local maxxcel = math_ceil(maxp.x / CELSIZE + CELOFFS) + 2
		local minzcel = math_floor(minp.z / CELSIZE + CELOFFS) - 2
		local maxzcel = math_ceil(maxp.z / CELSIZE + CELOFFS) + 2

		local found = {}
		for z = minzcel, maxzcel do
			local row = confroot[z]
			for x = minxcel, maxxcel do
				local cel = row[x]
				if maxp.x >= cel.x - RMAX and minp.x <= cel.x + RMAX
				and maxp.z >= cel.z - RMAX and minp.z <= cel.z + RMAX then
					found[#found + 1] = cel
				end
			end
		end

		if #found < 1 then return end

		for i = 1, #found do
			found[i].rd = loaddepth(found[i])
		end

		local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
		local area = VoxelArea:new({MinEdge = emin, MaxEdge = emax})
		local data = vm:get_data()

		local pidx = 1
		for y = minp.y, maxp.y do
			local cy = y / YSTEP + YOFFS
			local ci = math_floor(cy)
			local cw = cy - ci
			for i = 1, #found do
				local rd = found[i].rd
				local r = rd[ci] * (1 - cw) + rd[ci + 1] * cw
				found[i].rs = r * r
			end
			for i = 1, #found do
				local fx = found[i].x
				local fz = found[i].z
				local frs = found[i].rs
				for z = minp.z, maxp.z do
					for x = minp.x, maxp.x do
						if (x - fx) * (x - fx) + (z - fz) * (z - fz) <= frs then
							local i = area:index(x, y, z)
							if data[i] ~= airid then data[i] = srcid end
						end
					end
				end
			end
		end

		vm:set_data(data)
		vm:calc_lighting()
		vm:write_to_map()  
	end)
