-- LUALOCALS < ---------------------------------------------------------
local minetest, sz_pos, sz_util
    = minetest, sz_pos, sz_util
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- DEFAULT FURNACE INTEGRATION

-- Griddle API hook method for the furnace.
local function furnace_heated(pos, temp)
	-- Don't bother with griddles that aren't at least as hot as
	-- one with a single flame directly underneath.
	if temp < 2 then return temp end

	-- Get the current fuel status of the furnace.
	pos = sz_pos:new(pos)
	local meta = pos:meta()
	local fuel = meta:get_float("fuel_time")
	local max = meta:get_float("fuel_totaltime")

	-- If the furnace has already started burning, add
	-- fuel from the heat supplied, and return the heat
	-- that would not fit in the fuel reservoir.
	if fuel < max then
		local use = temp
		if use > fuel then use = fuel end
		meta:set_float("fuel_time", fuel - use)
		temp = temp - use
		return temp
	end

	-- If the furnace is not running, add the supplied
	-- heat to a "latent heat" counter.
	local latent = meta:get_float("latent_heat")
	latent = latent + temp

	-- If we've saved up enough heat, equivalent to a
	-- lump of coal, then start up the furnace from it,
	-- and return any leftover heat to the griddle.
	if latent >= 40 then
		meta:set_float("fuel_time", 0)
		meta:set_float("fuel_totaltime", 40)
		meta:set_float("latent_heat", 0)
		minetest.get_node_timer(pos):start(1)
		return latent - 40
	end

	-- If we haven't saved up enough heat, take all the
	-- available heat from the griddle and put it into
	-- our latent supply.
	meta:set_float("latent_heat", latent)
	return 0
end

-- Modify both of the default furnace nodes.
sz_util.modify_node("default:furnace", {
		on_griddle_heat = furnace_heated
	})
sz_util.modify_node("default:furnace_active", {
		on_griddle_heat = furnace_heated
	})
