-- LUALOCALS < ---------------------------------------------------------
local math, minetest, sz_pos, type
    = math, minetest, sz_pos, type
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- GRIDDLE NODE

local nodename = modname .. ":griddle"

-- The griddle processes a timer "tick" every 2 seconds.
local function griddle_tick(pos)
	pos = sz_pos:new(pos)
	pos:timer():start(2)

	-- Get the griddle's surrounding temperature.
	local temp = pos:heat_level()

	-- Calculate the total net temperature of the node, taking into account
	-- its previous temperature and specific heat.
	local meta = pos:meta()
	temp = (meta:get_float("temp") or 0) * 0.75 + temp * 0.25

	-- Damage any entities above the griddle from radiated/conducted heat.
	local above = pos:add(sz_pos.dirs.u)
	pos:add(sz_pos:xyz(0, 0.5, 0)):hitradius(1.5, temp)

	-- Look for a node above the griddle that provides an API to supply
	-- it heat from the griddle.  If it has one, send it our temperature
	-- and get the final temperature from it after it has "consumed" some
	-- of our heat.
	local heat = above:nodedef()["on_griddle_heat"]
	if heat then
		local ret = heat(above, temp, pos)
		if type(ret) == "number" then temp = ret end
	end

	-- Save the final temperature.
	meta:set_float("temp", temp)

	-- Show some smoke effects based on temperature.
	if temp >= 2 and above:is_empty() then
		above:smoke(math_floor(temp / 2), sz_pos.zero, {
				time = 2,
				maxpos = above:add(sz_pos:xyz(0.5, -0.25, 0.5))
			})
	end

	-- Griddles melt at high enough temperature.
	if temp >= 30 then pos:slagmelt() end
end

-- Register the actual griddle node.
minetest.register_node(nodename, {
		description = "Griddle",
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{ -0.5, 0.1666, -0.5, 0.5, 0.5, 0.5 },
				{ -0.1785, -0.5, -0.5, 0.1785, 0.1785, 0.5 },
				{ -0.5, -0.5, -0.1785, 0.5, 0.1785, 0.1785 }
			}
		},
		tiles = {
			"default_steel_block.png",
			"default_copper_block.png^sz_thermal_griddle_bottom.png",
			"default_steel_block.png^[lowpart:80:default_copper_block.png^sz_thermal_griddle_side.png"
		},
		groups = { cracky = 1 },
		slagdrops = {
			["default:steel_ingot"] = 4,
			["default:copper_ingot"] = 7
		},
		on_construct = griddle_tick,
		on_timer = griddle_tick,
		paramtype = "light"
	})

minetest.register_craft({
		output = modname .. ":griddle",
		recipe = {
			{ "", "stairs:slab_steelblock", "" },
			{ "", "stairs:slab_copperblock", "" },
			{ "default:copper_ingot", "default:copper_ingot", "default:copper_ingot" },
		}
	})
