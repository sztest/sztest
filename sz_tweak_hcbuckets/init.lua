-- LUALOCALS < ---------------------------------------------------------
local bucket, minetest, pairs, sz_pos, tonumber
    = bucket, minetest, pairs, sz_pos, tonumber
-- LUALOCALS > ---------------------------------------------------------

-- This only applies to classic fluids; buckets already work fine as
-- they are for finite liquids.
if minetest.setting_getbool("liquid_finite") then return end

local modname = minetest.get_current_modname()

-- Buckets don't pick up liquid nodes anymore, but just sample them.
-- Temporarily disable minetest.remove_node and minetest.add_node
-- while swinging the bucket; this is less network-intensive than
-- replacing the node afterwards.
local emname = "bucket:bucket_empty"
local empty = minetest.registered_items[emname]
if empty then
	local old_use = empty.on_use
	empty.on_use = function(itemstack, user, pointed_thing, ...)
		local def = sz_pos:new(pointed_thing.under):nodedef()
		if def and def.damage_per_second then
			user:set_hp(user:get_hp() - def.damage_per_second)
		end
		if def and def.groups and def.groups.sz_bucket_softcore then
			return old_use(itemstack, user, pointed_thing, ...)
		end
		local old_add = minetest.add_node
		minetest.add_node = function() end
		local old_remove = minetest.remove_node
		minetest.remove_node = function() end
		local function helper(...)
			minetest.add_node = old_add
			minetest.remove_node = old_remove
			return ...
		end
		return helper(old_use(itemstack, user, pointed_thing, ...))
	end
	minetest.register_craftitem(":" .. emname, empty)
end

-- Register an entity to remove liquid source nodes placed by
-- a bucket, after a delay.
local entname = modname .. ":liquid_remover"
minetest.register_entity(entname, {
		initial_properties = {
			hp_max = 1,
			physical = false,
			collide_with_objects = false,
			collisionbox = { 0, 0, 0, 0, 0, 0 },
			is_visible = false,
		},
		get_staticdata = function(self)
			return minetest.serialize({
					time = self.time,
					node = self.node,
				})
		end,
		on_activate = function(self, staticdata)
			local data = staticdata and minetest.deserialize(staticdata)
			if data then
				self.time = data.time
				self.node = data.node
			end
			self.object:set_armor_groups({ immortal = 1})
			self.object:setvelocity(sz_pos.zero)
			self.object:setacceleration(sz_pos.zero)
		end,
		on_step = function(self, dtime)
			local pos = self.object:getpos()
			self.node = self.node or minetest.get_node(pos)
			self.time = (self.time or (tonumber(minetest.setting_get("liquid_update"))
					or 1) * 2) - dtime
			if self.time <= 0 then
				local node = minetest.get_node(pos)
				if node and self.node and node.name == self.node.name then
					local def = minetest.registered_nodes[self.node.name]
					minetest.set_node(pos, { name = def
							and def.liquid_alternative_flowing
							or "default:water_flowing", param2 = 7 })
				end
				self.object:remove()
			end
		end
	})

-- After placing a liquid source node with a bucket, add a watcher entity
-- to remove that node after a short delay, simulating a finite quanitity
-- of liquid in the bucket.
for k, v in pairs(bucket.liquids) do
	local name = v.itemname
	local item = name and minetest.registered_items[name]
	local srcdef = v.source and minetest.registered_nodes[v.source]
	if item and not srcdef or not srcdef.groups or not srcdef.groups.sz_bucket_softcore then
		local old_place = item.on_place
		item.on_place = function(itemstack, user, pointed_thing, ...)
			local above = pointed_thing.above
			local under = pointed_thing.under

			-- Disallow placement if there is already a nearby source of
			-- the same fluid, to prevent players creating infinite springs
			-- by placing 2 buckets very quickly.
			local min = {
				x = (above.x < under.x and above.x or under.x) - 2,
				y = (above.y < under.y and above.y or under.y),
				z = (above.z < under.z and above.z or under.z) - 2,
			}
			local max = {
				x = (above.x > under.x and above.x or under.x) + 2,
				y = (above.y > under.y and above.y or under.y),
				z = (above.z > under.z and above.z or under.z) + 2,
			}
			if #minetest.find_nodes_in_area(min, max, v.source) > 0 then
				return
			end

			-- After standard liquid placement, search for the just-placed
			-- liquid, and add the delayed deletion entity to it.
			local function helper(...)
				local pos = sz_pos:new(pointed_thing.under)
				local node = pos:node_get()
				if not node or node.name ~= v.source then
					pos = sz_pos:new(pointed_thing.above)
					node = pos:node_get()
				end
				if node and node.name == v.source then
					minetest.add_entity(pos, entname)
				end
				return ...
			end
			return helper(old_place(itemstack, user, pointed_thing, ...))
		end
		minetest.register_craftitem(":" .. name, item)
	end
end
