-- LUALOCALS < ---------------------------------------------------------
local default, math, minetest, pairs, sz_pos, sz_util, type
    = default, math, minetest, pairs, sz_pos, sz_util, type
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- Heating a coal block on a griddle changes it into a block of heated
-- coal, which produces flammable gas.  The heated coal is itself a
-- dangerous igniter, so this process requires water cooling.

-- Function for handling heat supplied to a coal block from a griddle,
-- turning it into a hot coal block.  Heating via griddle only does
-- heat-up; cool-down is via abm.
local function coalblock_heated(pos, temp)
	if temp < 5 then return end
	pos = sz_pos:new(pos)
	if pos:node_get().name ~= modname .. ":coalblock_hot" then
		pos:node_swap({ name = modname .. ":coalblock_hot" })
	end
	pos:meta():set_float("hottime", 10)
end

-- Register the hot coal block itself, and add griddle hooks
-- to the default coal block with the griddle hook.
local function anim(name)
	return {
		name = name,
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 3
		}
	}
end
minetest.register_node(modname .. ":coalblock_hot", {
		description = "Hot Coal Block",
		tiles = {
			"default_coal_block.png",
			anim("default_lava_source_animated.png"),
			anim("sz_thermal_coalblock_hot.png")
		},
		drop = "default:coalblock",
		is_ground_content = true,
		groups = {
			cracky = 3,
			flammable = 1,
			igniter = 1,
			sz_coalgas_source = 1
		},
		sounds = default.node_sound_stone_defaults(),
		on_griddle_heat = coalblock_heated
	})
sz_util.modify_node("default:coalblock", {
		on_griddle_heat = coalblock_heated
	})

minetest.register_abm({
		nodenames = { modname .. ":coalblock_hot" },
		interval = 1,
		chance = 1,
		action = function(pos)
			pos = sz_pos:new(pos)
			local hottime = pos:meta():get_float("hottime")
			pos:meta():set_float("hottime", hottime - 1)
			if hottime < math_random() * 5 then
				pos:node_set({ name = "default:coalblock" })
				return
			end
			for k, v in pairs(sz_pos.dirs) do
				local p = pos:add(v)
				if p:node_get().name == "air" then
					p:node_set({ name = "sz_coalgas:gas_source" })
				end
			end
		end
	})
