-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_mininodes, sz_table, type
    = minetest, pairs, sz_mininodes, sz_table, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- Common logic to expand a partial registered item name into a full one.
local function fullname(namepart, subname)
	if namepart:gsub(":", "") == namepart then
		namepart = modname .. ":" .. namepart
	end
	if namepart:sub(-1) == "_" then
		namepart = namepart .. subname
	end
	return namepart
end

-- Helper function to do the actual mininode registration.
local function regmini(subname, namepart, desc, fixed, cuts, sawout, def, meltsto, nosun)
	local sawdrops = nil
	for k, v in pairs(sawout) do
		sawdrops = sawdrops or { }
		sawdrops[fullname(k, subname)] = v
	end

	local tiles = sz_mininodes.custom_overlay_get(
		{ def.tiles[1], def.tiles[2], def.tiles[5] }, cuts)

	local dropname = sz_mininodes.custom_drop_get(subname)

	local groups = sz_table.merge({ precision_craft = 1 }, def.groups)
	for k, v in pairs(groups) do
		local nk = k:gsub("mininode_[^_]+_", "mininode_" .. namepart .. "_")
		if k ~= nk then
			groups[nk] = v
			groups[k] = nil
		end
	end

	minetest.register_node(":" .. modname .. ":" .. namepart .. "_" .. subname, {
			description = desc,
			drop = modname .. ":" .. namepart .. "_" .. dropname,
			drawtype = "nodebox",
			node_box = {
				type = "fixed",
				fixed = fixed
			},
			tiles = tiles,
			sunlight_propagates = def.sunlight_propagates or not nosun,
			paramtype = "light",
			paramtype2 = "facedir",
			groups = groups,
			sounds = def.sounds,
			sawdrops = sawdrops,
			meltsto = meltsto,
			on_place = def.on_place
		})
end

-- Helper function to create each type of mininode for a given stair.  Note
-- that we cannot create them based on slabs alone because we need a non-custom
-- "back" texture to calculate correct mininode custom textures, and slabs
-- don't have an uncut "back" side.
function sz_mininodes.register_mininodes(subname, stairdef, recipeitem)

	-- Make sure we have an equivalent slab as well, since it's
	-- needed for some recipes and saw cuts.
	local slabname = "stairs:slab_" .. subname
	if not minetest.registered_items[slabname] then return end

	-- Helper to further shorten registration code.
	local function reg(namepart, descpart, fixed, cuts, sawout, meltqty, nosun)
		return regmini(subname, namepart,
			stairdef.description:gsub("Stair", descpart),
			fixed, cuts, sawout, stairdef, { [recipeitem] = meltqty }, nosun)
	end

	-- Register each mininode type.

	reg("dentil", "Dentil", { -0.5, -0.5, 0.5, 0, 0, 0 },
		{ "hv", "hv", "bb", "b", "b", "bb" },
		{ }, 1/8)

	reg("anulette", "Anulette", { -0.5, -0.5, 0.5, 0.5, 0, 0 },
		{ "h", "h", "b", "b", "b", "bb" },
		{ dentil_ = 2 }, 1/4)

	reg("corbel", "Corbel", {
			{ -0.5, -0.5, 0.5, 0.5, 0, 0 },
			{ -0.5, -0.5, 0, 0, 0, -0.5 },
			}, { "hv", "hv", "bl", "b", "b", "br" },
		{ anulette_ = 1, dentil_ = 1 }, 3/8)

	reg("chamfer", "Chamfer", {
			{ -0.5, -0.5, 0.5, 0.5, 0, 0 },
			{ -0.5, -0.5, 0, 0, 0, -0.5 },
			{ -0.5, 0, 0.5, 0, 0.5, 0 },
			}, { "hv", "hv", "bl", "r", "l", "br" },
		{ corbel_ = 1, dentil_ = 1 }, 0.5)


	reg("convex", "Convex Corner Stair", {
			{ -0.5, -0.5, 0.5, 0.5, 0, -0.5 },
			{ -0.5, 0, 0.5, 0, 0.5, 0 },
			}, { "hv", nil, "b", "r", "l", "b" },
		{ [slabname] = 1, dentil_ = 1 }, 5/8,
		true)

	reg("concave", "Concave Corner Stair", {
			{ -0.5, -0.5, 0.5, 0.5, 0, -0.5 },
			{ -0.5, 0, 0.5, 0.5, 0.5, 0 },
			{ -0.5, 0, 0, 0, 0.5, -0.5 },
			}, { "hv", nil, "l", nil, nil, "r" },
		{ [slabname] = 1, corbel_ = 1 }, 7/8,
		true)

	-- Now, register additive recipes for the mininodes to
	-- reconstruct them back into larger ones.

	local function recipe(output, ...)
		local inputs = { }
		for k, v in pairs({ ... }) do
			inputs[k] = fullname(v, subname)
		end
		output = fullname(output, subname)
		minetest.register_craft({
				type = "shapeless",
				output = output,
				recipe = inputs
			})
	end

	recipe("anulette_", "dentil_", "dentil_")
	recipe("corbel_", "dentil_", "anulette_")
	recipe("chamfer_", "dentil_", "corbel_")
	recipe("convex_", "dentil_", "stairs:slab_")
	recipe("convex_", "dentil_", "chamfer_")
	recipe("stairs:stair_", "dentil_", "convex_")
	recipe("concave_", "dentil_", "stairs:stair_")
	recipe(recipeitem, "dentil_", "concave_")

	recipe("stairs:slab_", "anulette_", "anulette_")
	recipe("convex_", "anulette_", "corbel_")
	recipe("stairs:stair_", "anulette_", "stairs:slab_")
	recipe("concave_", "anulette_", "convex_")
	recipe(recipeitem, "anulette_", "stairs:stair_")

	recipe("stairs:stair_", "corbel_", "corbel_")
	recipe("concave_", "corbel_", "stairs:slab_")
	recipe(recipeitem, "corbel_", "convex_")

	recipe(recipeitem, "stairs:slab_", "stairs:slab_")
	recipe(recipeitem, "chamfer_", "chamfer_")
end
