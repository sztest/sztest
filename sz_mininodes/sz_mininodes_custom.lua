-- LUALOCALS < ---------------------------------------------------------
local error, pairs, pcall, stairs, sz_mininodes, type
    = error, pairs, pcall, stairs, sz_mininodes, type
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- RE-PROCESS REGISTRATION

-- Leave behind a hook to re-register all stairs/slabs, so that custom
-- modifications to the registration function can be added by later
-- code, and they can apply to all previously-registered stairs without
-- all the rigamarole above.
function sz_mininodes.reregister_all()
	for subname, recipeitem in pairs(sz_mininodes.recipeitems) do
		stairs.register_stair_and_slab(subname, recipeitem)
	end
end
function sz_mininodes.reregister_defer(func, ...)
	local old = sz_mininodes.reregister_all
	sz_mininodes.reregister_all = function() end
	local function helper(ok, err, ...)
		sz_mininodes.reregister_all = old
		old()
		if ok then
			return err, ...
		else
			error(err)
		end
	end
	return helper(pcall(func, ...))
end

------------------------------------------------------------------------
-- CUSTOM DROPS

-- Original stairs and slabs always dropped themselves.  This is inconsistent
-- with game logic for their materials, e.g. stone normally drops as cobble,
-- and grass as dirt.  Add support to register alternative materials to drop
-- cut blocks as, as well.  Note that the dropped materials need to be registered
-- too.
local custom_drops = {}

-- Get the custom drop for a material type.
function sz_mininodes.custom_drop_get(name)
	return custom_drops[name] or name
end

-- Register a custom drop for a material type.
function sz_mininodes.custom_drop_set(name, val)
	if custom_drops[name] == val then return end
	custom_drops[name] = val
	sz_mininodes.reregister_all()
end

------------------------------------------------------------------------
-- CUSTOM TEXTURE OVERLAYS

-- When stairs are registered using these specific materials,
-- then the custom textures are added over the base ones to correct issues
-- with the appearance of finished product, e.g. replace bark of trees with
-- cross section of rings for portions cut away.
local custom_overlays = {}
local custom_bases = {}

-- Shortcut names for different "cuts" from a node.
local cuts_by_name = {
	l  = 0, -- Top left cut back.
	r  = 1, -- Top right cut back.
	b  = 2, -- Top half cut back.
	bl = 3, -- Bottom left cut back, top left excised.
	br = 4, -- Bottom right cut back, top right excised.
	bb = 5  -- Bottom half cut back, top half excised.
}

-- Process the custom overlay for a given base texture name and
-- a set of "cuts" (either names or numbers from cuts_by_name).
-- Returns the completed textures, with overlays for the given
-- cuts applied properly.
function sz_mininodes.custom_overlay_get(tiles, cuts)
	if not tiles then return tiles end
	local newtiles = { }
	for k = 1, 6 do
		local v = tiles[k] or tiles[#tiles]
		local cut = cuts[k]
		if cut and v then
			local t = v
			while type(t) == "table" do
				t = t.name
			end
			-- Note: the "identity transform" part is to create
			-- a marker for the "custom" part of the texture, so that
			-- if we re-register the node, we can strip off the
			-- old custom part before re-applying the new one.
			local i = t:find("^[transformI", 1, true)
			if i then t = t:sub(1, i - 1) end
			local txr = custom_overlays[t]
			local base = custom_bases[t]
			if txr or base then
				v = t .. "^[transformI"
				if base then v = v .. "^" .. base end
				if txr then
					v = v .. "^[transformI^("
					.. txr .. "^[verticalframe:6:"
					.. (cuts_by_name[cut] or cut) .. ")"
				end
			end
		end
		newtiles[k] = v
	end
	return newtiles
end

-- Register a "standard overlay" texture for a given
-- base texture name.  The "rebase" is an alternative base
-- texture to apply, such as overwriting dirt-with-grass
-- with dirt again, in case the base dirt-with-grass texture
-- doesn't fit right.
function sz_mininodes.custom_overlay_set(base, txr, rebase)
	custom_overlays[base] = txr
	if rebase then custom_bases[base] = rebase end
	sz_mininodes.reregister_all()
end
