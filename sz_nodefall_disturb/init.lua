-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, print, sz_pos, tostring
    = ipairs, math, minetest, print, sz_pos, tostring
local math_floor, math_random
    = math.floor, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local nodeupdate = minetest.check_for_falling or nodeupdate
if not nodeupdate then
	print("missing nodeupdate; " .. modname .. " disabled!")
	return
end

local function fallcheck(name, start)
	local target = start:scatter(64)
	local clr, pos = minetest.line_of_sight(start, target)
	if clr then return end
	pos = sz_pos:new(pos)

	local found = pos:nodes_in_area(2, "group:falling_node")
	if #found < 1 then return end
	pos = sz_pos:new(found[math_random(1, #found)])

	repeat pos.y = pos.y - 1 until not pos:groups().falling_node
	pos.y = pos.y + 1
	local prev = pos:node_get().name
	nodeupdate(pos)
	if pos:node_get().name ~= prev then
		print(modname .. ": " .. name .. " disturbed "
			.. prev .. " at " .. tostring(pos))
	end

end

local function queuechecks(qty, name, pos)
	if qty < 1 then return end
	minetest.after(0, function()
			for i = 1, qty do
				fallcheck(name, pos)
			end
		end)
end

local oldpos = {}
local qtys = {}
minetest.register_globalstep(function(dtime)
		for i, v in ipairs(minetest.get_connected_players()) do
			local name = v:get_player_name()

			local pos = sz_pos:new(v:getpos())
			local old = oldpos[name] or pos
			oldpos[name] = pos

			if v:get_player_control().sneak then return end

			local q = (qtys[name] or 0)
			+ (pos:sub(old):len() * 0.25)
			+ dtime * 0.05
			queuechecks(math_floor(q), name, pos)
			qtys[name] = q - math_floor(q)
		end
	end)

minetest.register_on_dignode(function(pos, oldnode, digger)
		local name = "(unknown)"
		if digger and digger.get_player_name then name = digger:get_player_name() end
		queuechecks(4, name, sz_pos:new(pos))
	end)
