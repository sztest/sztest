-- LUALOCALS < ---------------------------------------------------------
local math, minetest, pairs, sz_pos, sz_util
    = math, minetest, pairs, sz_pos, sz_util
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- COAL ORE GAS

-- Coal ore sometimes produces flammable gas, making basic mining a
-- little hazardous.  Gas is generated only on initial mapblock
-- generation (coal ore will not spontaneously start producing gas
-- e.g. when exposed by mining), and varies with depth.

local coalore = "default:stone_with_coal"
local srcname = "sz_coalgas:gas_source"

minetest.register_on_generated(function(minp, maxp)
	for _, pos in pairs(minetest.find_nodes_in_area(minp, maxp, { coalore })) do
		local prob = -pos.y
		if prob > 0 then
			if prob > 256 then prob = 256 end
			if math_random(1, 25600) < prob then
				for k, v in pairs(sz_pos.dirs) do
					local p = v:add(pos)
					if p:node_get().name == "air" then
						p:node_set({ name = srcname })
					end
				end
			end
		end
	end
end)

sz_util.modify_node(coalore, {
	groups = { flammable_gas_create = 1 }
})
