-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, math, minetest, pairs, sz_pos, sz_rotary,
      sz_table, unpack
    = default, ipairs, math, minetest, pairs, sz_pos, sz_rotary,
      sz_table, unpack
local math_abs
    = math.abs
-- LUALOCALS > ---------------------------------------------------------

local tb_pw_name = "sz_rotary:waterturbine_powered"
local tb_nopw_name = "sz_rotary:waterturbine"
local tb_frame_name = "sz_rotary:waterturbine_frame"

------------------------------------------------------------------------
-- HELPERS

-- Radius within which to find objects for build site obstruction checks.
local tb_place_obj_radius = sz_pos:xyz(2.5, 2.5, 1.5):len()

-- Pre-designed "plan" for the turbine.
local turbine_plan = sz_table:new()
local function turbine_plan_set(x, y, z, name)
	local t = sz_table:new()
	t.pos = sz_pos:xyz(x, y, z)
	t.name = name
	turbine_plan[t.pos:hash()] = t
end
-- Pre-fill the entire 5x5x3 with frames.
for x = -2, 2 do
	for y = 0, 2 do
		for z = -2, 2 do
			turbine_plan_set(x, y, z, tb_frame_name)
		end
	end
end
-- Carve out the inside 3x3x1.
for x = -1, 1 do
	for z = -1, 1 do
		turbine_plan_set(x, 1, z, "air")
	end
end
-- Create the inlet port.
turbine_plan_set(0, 2, 0, "air")
-- Create the 4 outlet ports.
turbine_plan_set(-2, 1, 1, "air")
turbine_plan_set(2, 1, -1, "air")
turbine_plan_set(-1, 1, -2, "air")
turbine_plan_set(1, 1, 2, "air")
-- Place the main turbine block.
turbine_plan_set(0, 0, 0, tb_nopw_name)

-- Helper to manage turbine sounds, such as spin-up, spin-down,
-- and powered/running.
local turbine_sounds = { }
local function turbinesound(pos, name, loop, noreplace)
	local hash = sz_pos.hash(pos)
	local now = minetest.get_gametime()

	local playing = turbine_sounds[hash]
	if playing then
		local notexp = playing.when >= now - 10
		if playing.name == name and notexp then
			return
		elseif playing.handle then
			if noreplace and notexp then return end
			minetest.sound_stop(playing.handle)
		end
	end

	if not name then
		turbine_sounds[hash] = nil
		return
	end

	local handle = minetest.sound_play(
		"sz_rotary_turbine_" .. name, {
			pos = pos,
			loop = loop,
			max_hear_distance = 128
		}
	)
	turbine_sounds[hash] = { name = name, handle = handle, when = now }
end

------------------------------------------------------------------------
-- POWER CHECK ROUTINE

local function tb_power_check(pos)
	pos = sz_pos:new(pos)

	-- Search within the multinode area for obstructions.  Determine
	-- if the machine is powered (flowing water), unpowered (not
	-- enough water), or unpowered and jammed (obstruction nodes or
	-- entities).
	local power = true
	local jam = false
	for k, v in pairs(turbine_plan) do
		local n = pos:add(v.pos):node_get()
		if n.name == "ignore" then return end
		if v.name == "air" then
			-- Anything that's not flowing water in the
			-- turbine powers down.
			if n.name ~= "default:water_flowing" then
				power = false
				-- Anything not either air or water
				-- in the turbine's designed airspaces
				-- jams it.
				if n.name ~= "air" then
					jam = true
					break
				end
			end
		elseif not sz_pos.zero:eq(v.pos) and n.name ~= v.name then
			-- Any missing part of the turbine disables
			-- it (but doesn't jam it).
			power = false
		end
	end

	-- Check for obstructing entities.
	local collide = pos:add(sz_pos.dirs.u):tangible_in_radius(1)
	for k, v in pairs(collide) do
		power = false
		jam = true
		break
	end

	if pos:node_get().name == tb_pw_name then
		-- If the turbine was powered, and is now jammed,
		-- fail catastrophically.  Break all nodes inside,
		-- and eject material.
		if jam then
			-- Shatter nodes in random order so that sfx limiting
			-- doensn't play all sfx in one corner.
			local keys = turbine_plan:keys()
			keys:shuffle()
			for i, k in ipairs(keys) do
				local v = turbine_plan[k]
				local p = pos:add(v.pos)
				p:shatter("water turbine obstruction")
			end
			turbinesound(pos, nil)
			return
		end

		-- Graceful powerdown if water stopped.
		if not power then
			pos:node_set({ name = tb_nopw_name })
			turbinesound(pos, "spindown")
		else
			turbinesound(pos, "running", true, true)
		end
	elseif power then
		-- Power up.
		pos:node_set({ name = tb_pw_name })
		local po = pos:rotpower_out():clear()
		po:dir_set(sz_pos.dirs.d, true)
		po.qty = 3
		po:save()

		turbinesound(pos, "spinup")
		minetest.after(7, function()
			local node = pos:node_get()
			if node and node.name == tb_pw_name then
				turbinesound(pos, "running", true)
			end
		end)
	end
end

------------------------------------------------------------------------
-- PLACEMENT HANDLER

-- Standard alert for blocked placement.
local function blockedmsg(placer)
	minetest.chat_send_player(placer:get_player_name(),
		"Water turbine placement area is obstructed!  Make sure the area is " ..
			"clear of structures and objects (including yourself).",
		false)
end
-- On attempting to place the node, make sure we have enough room to
-- build the whole superstructure, and construct all the nodes that
-- comprise the water turbine.
local function tb_on_place(itemstack, placer, pointed_thing)
	-- Handle right-click event on thing we're pointing at, if applicable,
	-- e.g. open/close doors instead of placing a node against it.
	local under = sz_pos:new(pointed_thing.under)
	local underdef = under:nodedef()
	local ctl = placer:get_player_control()
	if underdef.on_rightclick and not (ctl and ctl.sneak) then
		return underdef.on_rightclick(under, under:node_get(), placer, itemstack)
	end

	-- Figure out whether we're replacing the pointed node, or building on atop.
	local pos = underdef.buildable_to and under or sz_pos:new(pointed_thing.above)

	-- Make sure we can actually build where we've chosen to.
	if not pos:nodedef().buildable_to then return end

	-- Check the target area for nodes or entities.
	for k, v in pairs(turbine_plan) do
		if not pos:add(v.pos):nodedef().buildable_to then
			return blockedmsg(placer)
		end
	end
	for k, v in pairs(minetest.get_objects_inside_radius(pos, tb_place_obj_radius)) do
		local delta = pos:sub(v:getpos())
		if math_abs(delta.x) < 2.5 or math_abs(delta.z) < 2.5
			or delta.y < 0.5 and delta.y > -1.5 then
			return blockedmsg(placer)
		end
	end

	-- Build the complex multi-node structure from the plan, and record the location
	-- of the center point for each node.
	local ctr = pos:serialize()
	for i, v in pairs(turbine_plan) do
		local p = pos:add(v.pos)
		p:node_set({ name = v.name })
		p:meta():set_string("center", ctr)
	end

	-- Consume one item from the stack.
	if not minetest.setting_getbool("creative_mode") then
		itemstack:take_item()
	end
	return itemstack
end

------------------------------------------------------------------------
-- TURBINE NODE REGISTRATION

local function reg_turbine(name, def)
	def.description = def.description or "Water Turbine"
	def.drop = def.drop or tb_nopw_name
	def.groups = def.groups or { }
	def.groups.cracky = def.groups.cracky or 1
	def.groups.sz_waterturbine = def.groups.sz_waterturbine or 1
	def.groups.saw_delicate = def.groups.saw_delicate or 1
	def.rotary_power_check = def.rotary_power_check or tb_power_check
	def.sounds = def.sounds or default.node_sound_stone_defaults()
	def.on_place = def.on_place or tb_on_place
	return sz_rotary.register_node(name, def)
end

-- Register the powered and unpowered turbine node proper.
reg_turbine(tb_nopw_name, {
	tiles = {
		"sz_rotary_waterturbine_intake.png",
		"sz_rotary_stonebox_output.png",
		"sz_rotary_waterturbine_output.png",
		"sz_rotary_waterturbine_output.png",
		"sz_rotary_waterturbine_output.png",
		"sz_rotary_waterturbine_output.png",
	},
	groups = { sz_waterturbine_core = 1 },
	after_destruct = function(pos) turbinesound(pos) end
})
local tb_out_anim = sz_rotary.std_anim("sz_rotary_waterturbine_output_anim.png")
reg_turbine(tb_pw_name, {
	tiles = {
		sz_rotary.std_anim("sz_rotary_waterturbine_intake_anim.png", 1/4),
		sz_rotary.std_anim("sz_rotary_stonebox_output_anim.png"),
		"sz_rotary_waterturbine_output.png",
		"sz_rotary_waterturbine_output.png",
		"sz_rotary_waterturbine_output.png",
		"sz_rotary_waterturbine_output.png",
	},
	groups = { sz_waterturbine_core = 1 },
	after_destruct = function(pos) turbinesound(pos) end
})

-- Ensure power checs are done once every few seconds for turbines,
-- as we want to spin up promptly, and catch any jams.
minetest.register_abm({
	nodenames = { "group:sz_waterturbine_core" },
	interval = 5,
	chance = 1,
	action = function(pos)
		sz_pos:new(pos):rotary_power_check()
	end
})

------------------------------------------------------------------------
-- TURBINE FRAME NODE

-- Periodic turbine frame node check: make sure the central turbine
-- block is still present, and remove the frame node if it's gone.
local function tb_frame_check(pos)
	pos = sz_pos:new(pos)
	local node = pos:node_get()
	if node and node.name == "ignore" then return end
	local ctr = pos:meta():get_string("center")
	ctr = ctr and ctr ~= "" and sz_pos:new(minetest.deserialize(ctr))
	if not ctr or ctr:eq(pos) then return pos:node_set() end
	node = ctr:node_get()
	if node and node.name == "ignore" then return end
	if not ctr:groups().sz_waterturbine_core then return pos:node_set() end
end

-- Register the actual frame node.
reg_turbine(tb_frame_name, {
	description = "Water Turbine Frame",
	tiles = { "sz_rotary_stonebox_frame.png" },
	drop = "default:cobble",
	rotary_power_check = tb_frame_check,
	on_construct = function(...)
		-- After placing a frame, on the next tick, make sure it
		-- has a center turbine node.
		local args = {...}
		minetest.after(0, function()
			tb_frame_check(unpack(args))
		end)
	end,
	on_dig = function(pos, node, ...)
		-- On digging a turbine frame node, transfer the event to the
		-- central turbine node, so the player picks the whole thing up
		-- as one node.
		pos = sz_pos:new(pos)
		local ctr = pos:meta():get_string("center")
		ctr = ctr and ctr ~= "" and sz_pos:new(minetest.deserialize(ctr)) or pos
		local digfunc = ctr:eq(pos) and minetest.node_dig or ctr:nodedef().on_dig or minetest.node_dig
		digfunc(ctr, ctr:node_get(), ...)
	end
})
