-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- CRAFT ITEMS

minetest.register_craftitem("sz_rotary:gear_wood", {
	description = "Wooden Gear",
	inventory_image = "sz_rotary_gear_wood.png"
})

minetest.register_craftitem("sz_rotary:grate", {
	description = "Grate",
	inventory_image = "sz_rotary_grate.png"
})

minetest.register_craftitem("sz_rotary:turbine", {
	description = "Turbine",
	inventory_image = "sz_rotary_turbine.png"
})

minetest.register_craftitem("sz_rotary:stoneframe", {
	description = "Stone Frame",
	inventory_image = minetest.inventorycube(
		"sz_rotary_stonebox_frame.png",
		"sz_rotary_stonebox_frame.png",
		"sz_rotary_stonebox_frame.png"),
})

------------------------------------------------------------------------
-- CRAFT ITEM RECIPES

minetest.register_craft({
	output = "sz_rotary:turbine",
	recipe = {
		{ "default:steel_ingot", "", "" },
		{ "default:steel_ingot", "default:steel_ingot", "default:steel_ingot" },
		{ "", "", "default:steel_ingot" },
	}
})
minetest.register_craft({
	output = "sz_rotary:turbine",
	recipe = {
		{ "", "", "default:steel_ingot" },
		{ "default:steel_ingot", "default:steel_ingot", "default:steel_ingot" },
		{ "default:steel_ingot", "", "" },
	}
})
minetest.register_craft({
	output = "sz_rotary:turbine",
	recipe = {
		{ "", "default:steel_ingot", "default:steel_ingot" },
		{ "", "default:steel_ingot", "" },
		{ "default:steel_ingot", "default:steel_ingot", "" },
	}
})
minetest.register_craft({
	output = "sz_rotary:turbine",
	recipe = {
		{ "default:steel_ingot", "default:steel_ingot", "" },
		{ "", "default:steel_ingot", "" },
		{ "", "default:steel_ingot", "default:steel_ingot" },
	}
})

minetest.register_craft({
	output = "sz_rotary:gear_wood 4",
	recipe = {
		{ "", "group:stick", "" },
		{ "group:stick", "group:wood", "group:stick" },
		{ "", "group:stick", "" },
	}
})

minetest.register_craft({
	output = "sz_rotary:grate",
	recipe = {
		{ "group:stick", "group:stick" },
		{ "group:stick", "group:stick" },
	}
})

minetest.register_craft({
	output = "sz_rotary:stoneframe",
	recipe = {
		{ "default:stone", "default:stone", "default:stone" },
		{ "", "default:stone", "" },
		{ "default:stone", "default:stone", "default:stone" },
	}
})

------------------------------------------------------------------------
-- NODE RECIPES

minetest.register_craft({
	output = "sz_rotary:axle",
	recipe = {
		{ "sz_rotary:gear_wood", "group:stick", "sz_rotary:gear_wood" },
	}
})

minetest.register_craft({
	output = "sz_rotary:gearbox",
	recipe = {
		{ "group:wood", "sz_rotary:gear_wood", "group:wood" },
		{ "sz_rotary:gear_wood", "sz_rotary:gear_wood", "sz_rotary:gear_wood" },
		{ "group:wood", "sz_rotary:gear_wood", "group:wood" },
	}
})

minetest.register_craft({
	output = "sz_rotary:turnstile",
	recipe = {
		{ "", "group:stick", "" },
		{ "group:stick", "sz_rotary:gearbox", "group:stick" },
		{ "", "group:stick", "" },
	}
})

minetest.register_craft({
	output = "sz_rotary:waterturbine",
	recipe = {
		{ "sz_rotary:stoneframe", "", "sz_rotary:stoneframe" },
		{ "", "sz_rotary:turbine", "" },
		{ "sz_rotary:stoneframe", "sz_rotary:gearbox", "sz_rotary:stoneframe" },
	}
})

minetest.register_craft({
	output = "sz_rotary:waterpump",
	recipe = {
		{ "default:stone", "", "default:stone" },
		{ "sz_rotary:grate", "sz_rotary:turbine", "sz_rotary:grate" },
		{ "default:stone", "sz_rotary:gear_wood", "default:stone" },
	}
})

minetest.register_craft({
	output = "sz_rotary:rotor",
	recipe = {
		{ "default:desert_stone", "default:desert_stone", "default:desert_stone" },
		{ "default:stone", "sz_rotary:gearbox", "default:stone" },
		{ "default:stone", "sz_rotary:gear_wood", "default:stone" },
	}
})

minetest.register_craft({
	output = "sz_rotary:clutch",
	recipe = {
		{ "", "default:steel_ingot", "" },
		{ "stairs:slab_cobble", "sz_rotary:gear_wood", "stairs:slab_cobble" },
	}
})

minetest.register_craft({
	output = "sz_rotary:hopper",
	recipe = {
		{ "sz_stairs:corbel_wood", "sz_rotary:gear_wood", "sz_stairs:corbel_wood" },
		{ "sz_rotary:gear_wood", "", "sz_rotary:gear_wood" },
		{ "sz_stairs:corbel_wood", "sz_rotary:gear_wood", "sz_stairs:corbel_wood" },
	}
})
