-- LUALOCALS < ---------------------------------------------------------
local minetest, type
    = minetest, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- NODE RECIPES

minetest.register_craft({
	output = modname .. ":brazier",
	recipe = {
		{ "default:cobble", "default:coalblock", "default:cobble" },
		{ "default:cobble", "default:mese_crystal", "default:cobble" },
		{ "default:cobble", "default:cobble", "default:cobble" },
	}
})

minetest.register_craft({
	output = modname .. ":venturi",
	recipe = {
		{ "default:steel_ingot", "xpanes:bar", "default:steel_ingot" },
		{ "default:steel_ingot", "", "default:steel_ingot" },
		{ "default:steel_ingot", "xpanes:bar", "default:steel_ingot" },
	}
})

minetest.register_craft({
	output = modname .. ":griddle",
	recipe = {
		{ "", "stairs:slab_steelblock", "" },
		{ "", "stairs:slab_copperblock", "" },
		{ "default:copper_ingot", "default:copper_ingot", "default:copper_ingot" },
	}
})

minetest.register_craft({
	output = modname .. ":pyrosilica_clay",
	type = "shapeless",
	recipe = { "default:desert_sand", "default:sand", "default:clay" }
})

minetest.register_craft({
	output = modname .. ":crucible",
	recipe = {
		{ "sz_stairs:concave_pyrosilica_glass", "sz_stairs:corbel_pyrosilica_glass", "sz_stairs:concave_pyrosilica_glass" },
		{ "sz_stairs:corbel_pyrosilica_glass", "sz_stairs:dentil_pyrosilica_glass", "sz_stairs:corbel_pyrosilica_glass" },
		{ "sz_stairs:concave_pyrosilica_glass", "sz_stairs:corbel_pyrosilica_glass", "sz_stairs:concave_pyrosilica_glass" },
	}
})
