fungible single-function components
non-self-contained, sensitive to environment
emergent complexity
primitive industry (or steampunk) theme
focus on cooperative multiplayer survival
work with existing generated/imported maps
total conversion OR modular features
