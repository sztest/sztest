-- LUALOCALS < ---------------------------------------------------------
local math, minetest
    = math, minetest
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local animtime = 0
local animframe = 1
local animdirty
minetest.register_globalstep(function(d)
	animdirty = nil
	animtime = animtime + d
	if animtime >= 1 then
		animtime = animtime - math_floor(animtime)
		animframe = (animframe + 1) % 3
		animdirty = true
	end
end)
local function animtextures()
	return {
		"sz_rotary_windturbine_blades.png^[verticalframe:3:" .. animframe,
		"sz_rotary_windturbine_blank.png"
	}
end

local entname = modname .. ":fanblades"
local pw_name = modname .. ":turbine_powered"

minetest.register_entity(entname, {
	initial_properties = {
		hp_max = 1,
		physical = false,
		collide_with_objects = false,
		collisionbox = { 0, 0, 0, 0, 0, 0 },
		is_visible = true,
		visual = "upright_sprite",
		visual_size = { x = 5, y = 5 },
		textures = animtextures(),
	},
	on_step = function(self, dtime)
		local pos = self.object:getpos()
		local node = minetest.get_node(pos)
		if node.name ~= pw_name then
			self.object:remove()
		end
		if animdirty then
			self.object:set_properties({
				textures = animtextures()
			})
		end
	end
})

