-- LUALOCALS < ---------------------------------------------------------
local default, math, minetest, sz_facedir, sz_pos, sz_rotary, sz_table,
      tonumber
    = default, math, minetest, sz_facedir, sz_pos, sz_rotary, sz_table,
      tonumber
local math_cos, math_pi, math_random
    = math.cos, math.pi, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- CONSTANTS AND HELPERS

-- Light level from the sun, used to detect "open sky".
local LIGHT_SUN = 15

-- Node names.
local unpw_name = modname .. ":turbine"
local pw_name = modname .. ":turbine_powered"
local entname = modname .. ":fanblades"

-- Nodes allowed to obstruct the turbine without
-- jamming it.
local allow_obstruct = {
	air = true,
	["sz_rotary_transmit:axle"] = true,
	["sz_rotary_transmit:axle_powered"] = true
}

-- Calculate a sample of immediate turbine efficiency,
-- between -1 (for night operation) and 1 (for day).
-- Returns nil if jammed.
local function turbine_speed(pos, node, face)

	-- Check for jams.
	for dy = -2, 2 do
		for dx = -2, 2 do
			if dx ~= 0 or dy ~= 0 then
				local p = pos:add(face:left():scale(dx)):add(sz_pos:xyz(0, dy, 0))
				local n = p:node_get().name
				if not allow_obstruct[n] then return end
			end
		end
	end

	-- Do 5 random samples of cross-section.
	local hits = 0
	for n = 1, 5 do
		-- Measure a random distance in both fwd/back
		-- directions, and make sure the path is clear.
		local start = pos
		:add(face:left():scale(math_random(-2, 2)))
		:add(sz_pos:xyz(0, math_random(-2, 2), 0))
		local zmax = math_random(16)
		local fail, v
		for dz = 1, zmax do
			v = face:front():scale(dz)
			if not start:add(v):is_empty()
			or not start:sub(v):is_empty() then
				fail = true
				break
			end
		end

		-- Path must be clear, AND open to sky at the end.
		if not fail and start:add(v):light(0.5) == LIGHT_SUN
		and start:sub(v):light(0.5) == LIGHT_SUN then

			-- Search for land or water in one direction.
			for dy = 0, 16 do
				local p = start:add(v):add(sz_pos:xyz(0, -dy, 0))
				if not p:is_empty() then
					if p:groups().water then 
						hits = hits + 1 
					else
						hits = hits - 1 
					end
					break
				end
			end

			-- Search for land or water in the other.  Note that
			-- polarity of "hit" is reversed here, so opposite
			-- of other side will reinforce the effect.
			for dy = 0, 16 do
				local p = start:sub(v):add(sz_pos:xyz(0, -dy, 0))
				if not p:is_empty() then
					if p:groups().water then 
						hits = hits - 1
					else
						hits = hits + 1
					end
					break
				end
			end
		end
	end

	-- Up to 10 possible hits (2 sides * 5 samples)
	-- for maximum 1.0 efficiency.
	return hits / 10
end

-- Helper to turn a turbine on or off, including
-- adjusting power output, and changing node appearance.
local function turbine_setpower(pos, node, on)
	local po = pos:rotpower_out():clear()
	if on then
		po:dir_set(sz_pos.dirs.d, true)
		po.qty = 3
	end
	po:save()

	if on and node.name ~= pw_name then
		pos:node_swap({
				name = pw_name,
				param = node.param,
				param2 = node.param2
			})
		local dir = sz_facedir:from_param(node.param2):back().x * math_pi / 2;
		local ent = minetest.add_entity(pos, entname)
		ent:set_yaw(dir);
		local ent = minetest.add_entity(pos, entname)
		ent:set_yaw(dir + math_pi);
	elseif not on and node.name ~= unpw_name then
		pos:node_swap({
				name = unpw_name,
				param = node.param,
				param2 = node.param2
			})
	end
end

-- Complete periodic check run on a turbine by node timer.
local function turbine_check(pos)
	pos = sz_pos:new(pos)

	-- Loop timer.
	pos:timer():start(2)

	local node = pos:node_get()
	local face = sz_facedir:from_param(node.param2)
	local meta = pos:meta()

	-- Sample turbine efficiency, and shut it off
	-- if completely jammed.
	local hits = turbine_speed(pos, node, face)
	if hits == nil then
		turbine_setpower(pos, node)
		meta:set_float("eff", 0)
		return
	end

	-- Update cumulative efficiency, to smooth out sampling
	-- effects without having to scan the entire turbine
	-- area each iteration.
	local eff = tonumber(meta:get_string("eff")) or 0
	eff = eff * 0.8 + hits * 0.2
	meta:set_float("eff", eff)

	-- Calculate latent land/sea heat imbalance from from sun.
	local sun = -math_cos(math_pi * 2 * (minetest.get_timeofday() - 0.125))
	+ 0.15
	local speed = sun * eff

	-- Create dust motes to show air circulation for turbines
	-- that are positively biased.
	if speed > 0 then
		local initvel = face:back():scale(2 * speed)
		:add(sz_pos:xyz(0, -speed, 0))
		local acc = sz_pos:xyz(0, 0.25 * speed, 0)
		pos:smoke(1, sz_pos.zero, {
				time = 2,
				minpos = pos:add(face:front():scale(16 * speed))
				:add(face:left():scale(2))
				:add(sz_pos:xyz(0, -2 + 2 * speed, 0)),
				maxpos = pos
				:add(face:left():scale(-2))
				:add(sz_pos:xyz(0, 2 + 2 * speed, 0)),
				minvel = initvel,
				maxvel = initvel,
				minacc = acc,
				maxacc = acc,
				maxexptime = 8,
				maxsize = 3,
				collisiondetection = false
			})
	end

	-- Turbine won't actually operate below its
	-- stall speed, however; even maximally efficient
	-- turbines run less than half the day.
	turbine_setpower(pos, node, speed >= 0.2)
end

------------------------------------------------------------------------
-- NODE REGISTRSTION

-- Helper.
local function reg_turb(name, def)
	minetest.register_node(name, sz_table.mergedeep(def, {
				description = "Shore Wind Turbine",
				paramtype2 = "facedir",
				groups = {
					cracky = 1,
					sz_saw_delicate = 1,
					sz_rotary_windturbine = 1
				},
				legacy_facedir_simple = true,
				sounds = default.node_sound_stone_defaults(),
				on_timer = turbine_check,
				on_construct = turbine_check
			}))
end

-- Unpowered node.
reg_turb(unpw_name, {
		tiles = {
			"default_furnace_top.png",
			"sz_lib_rotary_stonebox_output.png",
			"default_furnace_top.png",
			"default_furnace_top.png",
			"sz_rotary_windturbine_front.png^[transformFX",
			"sz_rotary_windturbine_front.png"
		}
	})

-- Powered node.
reg_turb(pw_name, {
		tiles = {
			"default_furnace_top.png",
			sz_rotary.std_anim("sz_lib_rotary_stonebox_output_anim.png"),
			"default_furnace_top.png",
			"default_furnace_top.png",
			sz_rotary.std_anim("sz_rotary_windturbine_front_anim.png^[transformFX", 0.25),
			sz_rotary.std_anim("sz_rotary_windturbine_front_anim.png", 0.25)
		},
		groups = {not_in_creative_inventory = 1, sz_rotary_ambiance = 1}
	})

------------------------------------------------------------------------
-- CRAFTING

minetest.register_craft({
		output = unpw_name,
		recipe = {
			{ "default:stone", "default:stone", "default:stone" },
			{ "default:stone", modname .. ":fan", "default:stone" },
			{ "default:stone", "sz_rotary_transmit:gearbox", "default:stone" },
		}
	})
