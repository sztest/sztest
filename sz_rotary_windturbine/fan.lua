-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local fanname = modname .. ":fan"

minetest.register_craftitem(fanname, {
		description = "Turbine Blade",
		inventory_image = "sz_rotary_windturbine_fan.png"
	})

minetest.register_craft({
		output = fanname,
		recipe = {
			{ "default:steel_ingot", "", "" },
			{ "default:steel_ingot", "default:steel_ingot", "default:steel_ingot" },
			{ "", "", "default:steel_ingot" },
		}
	})
minetest.register_craft({
		output = fanname,
		recipe = {
			{ "", "", "default:steel_ingot" },
			{ "default:steel_ingot", "default:steel_ingot", "default:steel_ingot" },
			{ "default:steel_ingot", "", "" },
		}
	})
minetest.register_craft({
		output = fanname,
		recipe = {
			{ "", "default:steel_ingot", "default:steel_ingot" },
			{ "", "default:steel_ingot", "" },
			{ "default:steel_ingot", "default:steel_ingot", "" },
		}
	})
minetest.register_craft({
		output = fanname,
		recipe = {
			{ "default:steel_ingot", "default:steel_ingot", "" },
			{ "", "default:steel_ingot", "" },
			{ "", "default:steel_ingot", "default:steel_ingot" },
		}
	})
