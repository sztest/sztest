-- LUALOCALS < ---------------------------------------------------------
local default, minetest
    = default, minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_node(modname .. ":scaffold", {
		description = "Scaffold",
		drawtype = "glasslike",
		tiles = {{name = "sz_scaffold.png", backface_culling = false}},
		paramtype = "light",
		sunlight_propagates = true,
		walkable = false,
		climbable = true,
		is_ground_content = false,
		groups = {choppy = 3, oddly_breakable_by_hand = 3, flammable = 2, falling_node = 1},
		sounds = default.node_sound_wood_defaults()
	})

minetest.register_craft({
		output = modname .. ":scaffold 8",
		recipe = {
			{ "group:stick", "group:stick", "group:stick" },
			{ "group:stick", "group:stick", "group:stick" },
			{ "group:stick", "group:stick", "group:stick" }
		}
	})
