-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_util
    = minetest, pairs, sz_util
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
sz_util.modrenamed(modname, "sz_scythe")

local scythes = {}

local prefix = "default:sword_"
for k, def in pairs(minetest.registered_items) do
	if k:startswith(prefix) then
		local v = k:sub(#prefix + 1)
		local sn = modname .. ":scythe_" .. v
		scythes[sn] = true

		local txr = "default_" .. v .. "_block.png"
		if v == "wood" then txr = "default_wood.png" end

		local toolcap = minetest.deserialize(minetest.serialize(def.tool_capabilities))
		for gk, gv in pairs(toolcap.groupcaps) do
			gv.uses = gv.uses * 2
			for tk, tv in pairs(gv.times) do
				gv.times[tk] = tv * 0.8
			end
		end

		minetest.register_tool(sn, {
				description = def.description:gsub("Sword", "Scythe"),
				inventory_image = txr
				.. "^sz_tool_scythe_mask.png^[makealpha:255,0,255^sz_tool_scythe_handle.png",
				tool_capabilities = toolcap,
				sound = def.sound
			})
		minetest.register_craft({
				output = sn,
				recipe = {
					{ "default:sword_" .. v, "group:stick" },
					{ "group:stick", "group:stick" },
					{ "", "group:stick" }
				},
			})
	end
end

local log = minetest.chat_send_all

local function scytheuse(pos, node, digger, ...)
	if not digger or not digger.get_wielded_item then return end

	local wield = digger:get_wielded_item()
	if not wield or not scythes[wield:get_name()] then return end

	local def = minetest.registered_nodes[node.name]
	if not def or not def.groups or not def.groups.snappy then return end

	local min = {x = pos.x - 2, y = pos.y, z = pos.z - 2}
	local max = {x = pos.x + 2, y = pos.y, z = pos.z + 2}
	for k, v in pairs(minetest.find_nodes_in_area(min, max, {node.name})) do	
		minetest.node_dig(v, node, digger, ...)
	end
end

local digmask
minetest.register_on_dignode(function(...)
		if digmask then return end
		digmask = true
		scytheuse(...)
		digmask = nil
	end)
