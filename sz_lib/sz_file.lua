-- LUALOCALS < ---------------------------------------------------------
local error, io, minetest, pairs, sz_file, table, type
    = error, io, minetest, pairs, sz_file, table, type
local io_close, io_open, table_concat
    = io.close, io.open, table.concat
-- LUALOCALS > ---------------------------------------------------------

-- This is a helper class for wrapping around a plain text "database"
-- file.

------------------------------------------------------------------------
-- CONSTRUCTORS AND STATIC PROPERTIES

-- Concatenate path components.
local function cat(...)
	local t = {...}
	for k, v in pairs(t) do
		if type(v) == "table" then
			t[k] = cat(v)
		end
	end
	return table_concat(t, "/")
end
sz_file.cat = cat

-- Create a path by concatenating components.
function sz_file:mk(...)
	local t = {...}
	if #t < 1 then error("invalid empty path") end
	local f = t[#t]
	t[#t] = nil
	return sz_file:new({
			dir = cat(t),
			file = f,
			path = cat(t, f)
		})
end

-- Create within certain starting dirs.
function sz_file:world(...)
	return sz_file:new(
		minetest.get_worldpath(),
		...)
end
function sz_file:mod(...)
	return sz_file:new(
		minetest.get_modpath(minetest.get_current_modname()),
		...)
end
function sz_file:worldmod(...)
	return sz_file:new(
		minetest.get_worldpath(),
		minetest.get_modpath(minetest.get_current_modname()),
		...)
end

------------------------------------------------------------------------
-- FILE IO FUNCTIONS

-- Ensure the specified dir exists.
function sz_file:mkdir()
	return minetest.mkdir(self.dir)
end

-- Read content as raw text.
function sz_file:readtext()
	local f = io_open(self.path)
	if f then
		local d = f:read("*all")
		io_close(f)
		return d
	end
end

-- Read content as a serialized object.
function sz_file:readobj()
	local s = self:readtext()
	if s then return minetest.deserialize(s) end
end

-- Write content as raw text.
function sz_file:writetext(x)
	self:mkdir()
	return minetest.safe_file_write(self.path, x)
end

-- Write content as serialized.
function sz_file:writeobj(x)
	return self:writetext(minetest.serialize(x))
end
