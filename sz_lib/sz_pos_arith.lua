-- LUALOCALS < ---------------------------------------------------------
local math, sz_pos
    = math, sz_pos
local math_floor, math_random, math_sqrt
    = math.floor, math.random, math.sqrt
-- LUALOCALS > ---------------------------------------------------------

-- Return true if two positions are equal.
function sz_pos:eq(pos)
	if self == pos then return true end
	return self.x == pos.x and self.y == pos.y and self.z == pos.z
end

-- Round to nearest integer coordinates.
function sz_pos:round()
	return sz_pos:new({
			x = math_floor(self.x + 0.5),
			y = math_floor(self.y + 0.5),
			z = math_floor(self.z + 0.5)
		})
end

-- Vector addition.
function sz_pos:add(pos)
	return sz_pos:new({
			x = self.x + pos.x,
			y = self.y + pos.y,
			z = self.z + pos.z
		})
end

-- Locate a random position within the given node space.  Note that we
-- actually scatter a little less than the full node size, so that items
-- don't get hung up on ledges.
function sz_pos:scatter(scale)
	scale = scale or 0.5
	return self:round():add({
			x = (math_random() - 0.5) * scale,
			y = (math_random() - 0.5) * scale,
			z = (math_random() - 0.5) * scale
		})
end

-- Vector subtraction.  A shortcut (both syntactically and computationally)
-- for sz_pos:add(pos:neg())
function sz_pos:sub(pos)
	return sz_pos:new({
			x = self.x - pos.x,
			y = self.y - pos.y,
			z = self.z - pos.z
		})
end

-- Inverse vector, i.e. negate each coordinate.
function sz_pos:neg(pos)
	return sz_pos:new({ x = -self.x, y = -self.y, z = -self.z })
end

-- Vector scalar multiplication.
function sz_pos:scale(k)
	return sz_pos:new({
			x = self.x * k,
			y = self.y * k,
			z = self.z * k
		})
end

-- Vector dot multiplication.
function sz_pos:dot(pos)
	return self.x * pos.x
	+ self.y * pos.y
	+ self.z * pos.z
end

-- Vector cross multiplication.
function sz_pos:cross(pos)
	return sz_pos:new({
			x = self.y * pos.z - self.z * pos.y,
			y = self.z * pos.x - self.x * pos.z,
			z = self.x * pos.y - self.y * pos.x
		})
end

-- Get the euclidian length of the vector.
function sz_pos:len()
	return math_sqrt(self:dot(self))
end

-- Return a vector in the same direction as the original, but whose
-- length is either zero (for zero-length vectors) or one (for any other).
function sz_pos:norm()
	local l = self:len()
	if l == 0 then return self end
	return self:scale(1 / l)
end

-- Find the cardinal unit vector (one of 6 directions) that most closely
-- matches the general direction of this vector.
function sz_pos:dir()
	local function bigz()
		if self.z >= 0 then
			return sz_pos.dirs.n
		else
			return sz_pos.dirs.s
		end
	end
	local xsq = self.x * self.x
	local ysq = self.y * self.y
	local zsq = self.z * self.z
	if xsq > ysq then
		if zsq > xsq then
			return bigz()
		else
			if self.x >= 0 then
				return sz_pos.dirs.e
			else
				return sz_pos.dirs.w
			end
		end
	else
		if zsq > ysq then
			return bigz()
		else
			if self.y >= 0 then
				return sz_pos.dirs.u
			else
				return sz_pos.dirs.d
			end
		end
	end
end

-- Get the absolute value of each coordinate.  This can be used to
-- convert a vector into a 3D "size" for e.g. a bounding box.
function sz_pos:abs()
	return sz_pos:new({
			x = (self.x >= 0) and self.x or -self.x,
			y = (self.y >= 0) and self.y or -self.y,
			z = (self.z >= 0) and self.z or -self.z
		})
end
