-- LUALOCALS < ---------------------------------------------------------
local sz_util
    = sz_util
-- LUALOCALS > ---------------------------------------------------------

sz_util:loadlibs_prefix("sz_util_",
	"luameta",
	"itemmod",
	"shatter"
)
