-- LUALOCALS < ---------------------------------------------------------
local minetest, string, type
    = minetest, string, type
local string_len, string_sub
    = string.len, string.sub
-- LUALOCALS > ---------------------------------------------------------

local bitemname = "__builtin:item"
local bitem = minetest.registered_entities[bitemname]

local function copyfield(from, to)
	to.sz_data = from.sz_data
end

-- Upon deserializing entity data, restore the custom field.
local old_activate = bitem.on_activate
bitem.on_activate = function(self, staticdata, ...)
	old_activate(self, staticdata, ...)
	if string_sub(staticdata, 1, string_len("return")) == "return" then
		local data = minetest.deserialize(staticdata)
		if data and type(data) == "table" then copyfield(data, self) end
	end
	self.sz_data = self.sz_data or {}
end

-- Upon serializing entity date, save the custom field.
local old_getstatic = bitem.get_staticdata
bitem.get_staticdata = function(self, ...)
	local staticdata = old_getstatic(self, ...)
	if string_sub(staticdata, 1, string_len("return")) == "return" then
		local data = minetest.deserialize(staticdata)
		if data and type(data) == "table" then copyfield(self, data) end
		staticdata = minetest.serialize(data)
	end
	return staticdata
end

minetest.register_entity(":"..bitemname, bitem)
