-- LUALOCALS < ---------------------------------------------------------
local minetest, sz_pos, sz_util, type
    = minetest, sz_pos, sz_util, type
-- LUALOCALS > ---------------------------------------------------------

-- Shortcuts for some minetest utility functions.
sz_pos.node_swap = minetest.swap_node
sz_pos.light = minetest.get_node_light
sz_pos.timer = minetest.get_node_timer
sz_pos.drops = minetest.get_node_drops
sz_pos.meta = minetest.get_meta
sz_pos.node_near = minetest.find_node_near

-- Get the node at this position.
function sz_pos:node_get(withmeta)
	if withmeta then
		return minetest.get_node(self),
		sz_util.meta_to_lua(self:meta())
	end
	return minetest.get_node(self)
end

-- Change the node at this position.
function sz_pos:node_set(n, luameta)
	minetest.set_node(self, n or { name = "air" })
	if luameta then
		sz_util.lua_to_meta(luameta, self:meta())
	end
end

-- Get the inventory for this node position.
function sz_pos:inv()
	return minetest.get_inventory({type = "node", pos = self})
end

-- Copy a node, including metadata.
function sz_pos:node_copyto(dest)
	return sz_pos:new(dest):node_set(self:node_get(true))
end

-- Move a node, including metadata, and leaving
-- the specified content, or air, in its place.
function sz_pos:node_moveto(dest, node, luameta)
	self:node_copyto(dest)
	return self:node_set(node, luameta)
end

-- Trade 2 node positions, including metadata.
function sz_pos:node_trade(dest)
	dest = sz_pos:new(dest)
	return self:node_moveto(dest, dest:node_get(true))
end
