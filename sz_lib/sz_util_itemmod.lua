-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, sz_table, sz_util, type
    = ipairs, minetest, pairs, sz_table, sz_util, type
-- LUALOCALS > ---------------------------------------------------------

-- Merge modifications into a node definition.  This works for current
-- and future registrations, by way of intercepting the
-- minetest.register_node method.
local itemmods = {}
function sz_util.modify_item(name, mod)
	-- Mods can be a table, to be merged over the original;
	-- convert to a function.
	if type(mod) == "table" then
		local modtbl = mod
		mod = function(old) return sz_table.mergedeep(modtbl, old) end
	end

	-- Mod functions should only apply to each item type once.
	local oldmod = mod
	local modsdone = {}
	mod = function(def, name, ...)
		if modsdone[name] then return def end
		modsdone[name] = true
		return oldmod(def, name, ...)
	end

	-- Add the mod to the mods table, for future matching
	-- registrations.
	local mods = itemmods[name]
	if not mods then
		mods = sz_table:new()
		itemmods[name] = mods
	end
	mods:insert(mod)

	-- Apply the mod to any existing registrations.
	local function modold(name, old)
		local mn = minetest.get_current_modname()
		if name:sub(1, mn:len() + 1) ~= (mn .. ":")
		and name:sub(1, 1) ~= ":" then
			name = ":" .. name
		end
		minetest.register_item(name, mod(old, name))
	end
	if name == "*" then
		for k, v in pairs(minetest.registered_items) do
			if k ~= "ignore" then modold(k, v) end
		end
	elseif minetest.registered_items[name] then
		modold(name, minetest.registered_items[name])
	end
end
sz_util.modify_node = sz_util.modify_item

local oldreg = minetest.register_item
minetest.register_item = function(name, def, ...)
	local function applymods(mods)
		if not mods then return end
		for i, v in ipairs(mods) do
			def = v(def, name)
		end
	end
	applymods(itemmods[name])
	applymods(itemmods["*"])
	return oldreg(name, def, ...)
end

function sz_util.modrenamed(newname, ...)
	local olds = {...}
	sz_util.modify_item("*", function(def, name)
			if name:startswith(newname .. ":") then
				for _, v in ipairs(olds) do
					minetest.register_alias(name:gsub(".*:", v .. ":"), name)
				end
			end
			return def
		end)
end
