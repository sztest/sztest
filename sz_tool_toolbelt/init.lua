-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_util, type
    = minetest, pairs, sz_util, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
sz_util.modrenamed(modname, "sz_toolbelt")

local beltname = modname .. ":belt"

------------------------------------------------------------------------
-- RESTORE INVENTORY ON RELEASE

local restore = {}

minetest.register_globalstep(function(dtime)
		local newrest = {}
		for k, v in pairs(minetest.get_connected_players()) do
			local n = v:get_player_name()
			local r = restore[n]
			if r then
				if v:get_player_control().LMB then
					newrest[n] = r
				else
					r(v)
				end
			end
		end
		restore = newrest
	end)

------------------------------------------------------------------------
-- UTILITY TO FIND BEST TOOL

local function toolcap(t, ng)
	local c = t:get_tool_capabilities()
	if not c then return end
	local l = ng.level or 0
	for k, v in pairs(c.groupcaps) do
		local gn = ng[k]
		if v.maxlevel >= l and gn and v.times[gn] then
			local n = t:get_name();
			return {
				caps = v,
				lv = v.maxlevel,
				group = k,
				gnum = gn,
				time = v.times[gn],
				wear = t:get_wear(),
				cheap = n:endswith("_stone") or n:endswith("_wood")
			}
		end
	end
end

local function invswap(inv, n1, i1, n2, i2)
	local s1 = inv:get_stack(n1, i1)
	local s2 = inv:get_stack(n2, i2)
	inv:set_stack(n1, i1, s2)
	inv:set_stack(n2, i2, s1)
end

local function better(a, b)
	if not b then return end
	if not a then return true end

	-- Cheapest tool (or stone)...
	if not b.cheap and a.lv < b.lv then return end
	if b.lv < a.lv then return true end

	-- ...then fastest tool...
	if a.time < b.time then return end
	if a.time > b.time then return true end

	-- ...then least worn (wear-leveling)
	if a.wear < b.wear then return end
	if a.wear > b.wear then return true end
end

------------------------------------------------------------------------
-- SWAP TOOLS ON PUNCHING

local function restorepname(user)
	if not user or not user:is_player() then return end

	local pname = user:get_player_name()

	local rf = restore[pname]
	if rf then
		rf(user)
		restore[pname] = nil
	end

	return pname
end

minetest.register_on_punchnode(function(pos, node, user, pointed)
		local pname = restorepname(user)
		if not pname then return end

		if user:get_wielded_item():get_name() ~= beltname then return end

		local n = minetest.get_node_or_nil(pos)
		if not n then return end
		local r = minetest.registered_nodes[n.name]
		if not r then return end
		local ng = r.groups or {}

		local best
		local inv = user:get_inventory()
		for i = 1, inv:get_size("main") do
			local t = toolcap(inv:get_stack("main", i), ng)
			if better(best, t) then
				best = t
				best.i = i
			end
		end
		if best then
			local wl = user:get_wield_list()
			local wi = user:get_wield_index()
			if wl ~= "main" or wi ~= best.i then
				restore[pname] = function(p)
					return invswap(p:get_inventory(), wl, wi, "main", best.i)
				end
				return invswap(inv, wl, wi, "main", best.i)
			end
		end
	end)

minetest.register_on_dignode(function(_, _, user)
		restorepname(user)
	end)

------------------------------------------------------------------------
-- TORCH PLACEMENT ON RIGHTCLICK

local torch = "default:torch"

local torch_place = minetest.registered_nodes[torch].on_place or minetest.item_place

local function torchify(stack, user, pointed, ...)
	local pname = restorepname(user)
	if not pname then return minetest.item_place(stack, user, pointed, ...) end

	local best
	local inv = user:get_inventory()
	for i = 1, inv:get_size("main") do
		local t = inv:get_stack("main", i)
		if t:get_name() == torch then
			local q = t:get_count()
			if not best or q <= best.q then
				best = {
					s = t,
					q = q,
					i = i
				}
			end
		end
	end
	if not best then return minetest.item_place(stack, user, pointed, ...) end

	local repl = torch_place(best.s, user, pointed)
	inv:set_stack("main", best.i, repl)
end

------------------------------------------------------------------------
-- TOOLBELT ITEM AND RECIPES

minetest.register_tool(beltname, {
		description = "Tool Belt",
		inventory_image = "sz_tool_toolbelt.png",
		wield_image = "wieldhand.png",
		wield_scale = {x = 1, y = 1, z = 2.5},
		on_place = torchify
	})

minetest.register_craft({
		output = beltname,
		type = "shapeless",
		recipe = { "default:pick_stone", "default:axe_stone", "default:sword_stone",
			"default:shovel_stone", "group:wool", "default:steel_ingot"
		},
	})
