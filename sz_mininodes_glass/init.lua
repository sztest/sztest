-- LUALOCALS < ---------------------------------------------------------
local sz_mininodes
    = sz_mininodes
-- LUALOCALS > ---------------------------------------------------------

local reg = sz_mininodes.register

sz_mininodes.reregister_defer(function()
		reg("default", "glass", nil, { "sz_mininodes_glass_glass.png" })
		reg("default", "obsidian_glass", nil, { "sz_mininodes_glass_obsidian_glass.png" })
	end)
