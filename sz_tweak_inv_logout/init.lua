-- LUALOCALS < ---------------------------------------------------------
local io, math, minetest, pairs, sz_pos, sz_stuffpile
    = io, math, minetest, pairs, sz_pos, sz_stuffpile
local io_close, io_open, math_ceil
    = io.close, io.open, math.ceil
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- UTILITY FUNCTIONS

-- Drop all items, attempting to immediately settle them into piles (to
-- avoid item_entity_ttl jeopardy).  Try to save original item positions
-- too, so we can restore on login.
local function dropall(player)
	local gotany
	local data = { }
	local pn = player:get_player_name()
	local pos = sz_pos:new(player:getpos())
	pos.y = math_ceil(pos.y) + 0.5
	local inv = player:get_inventory()
	for _, n in pairs({ "main", "craft" }) do
		data[n] = { }
		for i = 1, inv:get_size(n) do
			local stack = inv:get_stack(n, i)
			data[n][i] = stack:to_string()
			if stack:get_count() > 0 then
				gotany = true
				if sz_stuffpile then
					stack = sz_stuffpile.settle_search(pos, stack, pn)
				end
			end
			if stack:get_count() > 0 then
				pos:item_eject(stack)
			end
		end
		inv:set_list(n, { })
	end
	if gotany then
		local p = minetest.get_worldpath() .. "/" .. modname
		minetest.mkdir(p)
		p = p .. "/" .. player:get_player_name() .. ".txt"
		minetest.safe_file_write(p, minetest.serialize(data))
	end
end

-- Find all nearby items and try to pick them back up again.
local function pickupall(player, pos)
	if player:get_hp() < 1 then return end

	local radius = sz_pos:xyz(1.5, 1.5, 1.5)
	local minpos = pos:add(radius:neg())
	local maxpos = pos:add(radius)

	for i, v in pairs(minetest.find_nodes_in_area(
			minpos, maxpos, { "group:items_on_ground" })) do
		v = sz_pos:new(v)
		local punch = v:nodedef().on_punch
		if punch then
			punch(v, v:node_get(), player)
		else
			minetest.node_punch(v, v:node_get(), player)
		end
	end

	for i, v in pairs(minetest.get_objects_inside_radius(pos, 4)) do
		if not v:is_player() then
			local rpos = sz_pos:new(v:getpos()):round()
			if rpos.x > minpos.x and rpos.x < maxpos.x
			and rpos.y > minpos.y and rpos.y < maxpos.y
			and rpos.z > minpos.z and rpos.z < maxpos.z then
				v:punch(player, 1, { })
			end
		end
	end	

	-- Attempt to restore inventory order, if saved.
	local f = io_open(minetest.get_worldpath() .. "/" .. modname
		.. "/" .. player:get_player_name() .. ".txt")
	if f then
		local data = f:read("*all")
		io_close(f)
		data = minetest.deserialize(data)
		local inv = player:get_inventory()
		local function invfind(ss)
			for _, n in pairs({ "main", "craft" }) do
				for i = 1, inv:get_size(n) do
					if inv:get_stack(n, i):to_string() == ss then
						return n, i
					end
				end
			end
		end
		for n, l in pairs(data) do
			for i, s in pairs(l) do
				local ss = inv:get_stack(n, i):to_string()
				if ss ~= s then
					local nn, ni = invfind(s)
					if nn then
						inv:set_stack(nn, ni, ss)
						inv:set_stack(n, i, s)
					end
				end
			end
		end
	end
end

-- Pickup all nearby items, if the local area is loaded.  If not, wait for
-- it to be loaded, and then pick up items as soon as it is.
local function pickupall_loop(player, pos)
	if pos:node_get().name == "ignore" then
		minetest.after(0, function() pickupall_loop(player, pos) end)
	else
		pickupall(player, pos)
	end
end

------------------------------------------------------------------------
-- PLAYER EVENT HANDLERS

-- When a player logs out, drop all inventory items, so they're
-- preserved in the world and not locked away in a non-corporeal
-- player.
minetest.register_on_leaveplayer(function(player)
		dropall(player)
	end)

-- When shutting down the server, all players are disconnected, so
-- process drop-everything logic for all currently-connected players.
minetest.register_on_shutdown(function()
		for k, v in pairs(minetest.get_connected_players()) do
			dropall(v)
		end
	end)

-- Upon logging back in, attempt to pickup all nearby items automatically,
-- so we don't end up leaving them in the wilderness and forgetting where
-- they are, after an inopportune disconnect.
minetest.register_on_joinplayer(function(player)
		local pos = sz_pos:new(player:getpos())
		minetest.after(0, function() pickupall_loop(player, pos) end)
	end)
