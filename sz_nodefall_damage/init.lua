-- LUALOCALS < ---------------------------------------------------------
local math, minetest, print, sz_pos
    = math, minetest, print, sz_pos
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local fallname = "__builtin:falling_node"
local fallnode = minetest.registered_entities[fallname]

if not fallnode then
	print("missing " .. fallname .. "; " .. modname .. " disabled!")
	return
end

local dmg = {}

local function dmggc()
	dmg = {}
	minetest.after(10, dmggc)
end
dmggc()

local oldtick = fallnode.on_step
fallnode.on_step = function(self, dtime, ...)
	local pos = sz_pos:new(self.object:getpos())
	local vel = sz_pos:new(self.object:getvelocity())

	local q = dmg[self] or 0
	q = q + vel:dot(vel) * dtime
	if q > 1 then
		local n = math_floor(q)
		pos:add(sz_pos.dirs.d):hitradius(1, n)
		q = q - n
	end
	dmg[self] = q

	return oldtick(self, dtime, ...)
end

minetest.register_entity(":" .. fallname, fallnode)
